import numpy as np

'''
Using an algorithm based on a ternary search algorithm proved to be unsuccessful because of the noise the algorithm often
concentrated on areas away from the global maximum. Therefore, we instead focused on the Zooming algorithm.
'''



class TernaryEstimation:

	def __init__(self, eval_fun, domain, discount, discrete_min=0.01):

		self.eval_fun = eval_fun
		self.domain = domain
		self.discount = discount


		
		# Ternary estimation should be self-guided
		
		# self.discret_min = discret_min
		# self.branching = branching
		# self.bins = [ [] for _ in range(branching) ]
		
		self.domain_size = domain[1] - domain[0]
		self.discret_min = discret_min

		self.memory = [ [] for _ in range(int(domain_size / self.discrete_min)) ]
		self.stats = np.zeros(len(self.memory))

		self.dicount_arr = np.array([self.discount**t for t in range(len(self.memory), step=-1)])

		x_init = self.domain[0] + (self.domain_size / 2)
		y_init = eval_fun(x_init)

		self._update_memory(x_init, y_init)

		# The best and its neighbors (left, right)
		self.best = {'center': x_init, 'left': None, 'right': None}

		

	def _idx(self, x):
		rounded = np.round(x, 2)
		return int((rounded - self.domain[0]) / self.discrete_min)

	def _update_memory(self, x, y):
		for mem_idx in self.memory:
			mem_idx = [ self.discount * x for x in mem_idx ]


		idx = self._idx(x)
		
		self.memory[idx].append(y)
		# self.memory[idx][:-1] = [ self.discount * x for x in self.memory[idx][:-1] ]

		

		self.stats[idx] = np.argmax(self.memory[idx])


		

	def next(self, k):

		if k < 2:
			x_init2 = self.domain[0] + (self.domain_size / 4)
			y_init2 = eval_fun(x_init2)

			self._update_memory(x_init2, y_init2)

			# Keep track of 2 largest
			if y_init2 > self.stats[self.best_2_idx[0]]:
				self.best = {'center': x_init2, 'left': None, 'right': self.best['center']}
			else:
				self.best = {'center': self.best['center'], 'left': x_init2, 'right': None}

		else:
			# If left neighbor non-existing --> explore on the left side -- ie optimistically
			if self.best['left'] is None:
				new_x = self.domain[0] + ((self.best['left'] - self.domain[0]) / 2)
				chosen_size = 'left None'
			# If right neighbor non-existing
			elif self.best['right'] is None:
				new_x = self.best['right'] + ((self.domain[1] - self.best['right']) / 2)
				chosen_size = 'right None'
			else:

				if self.best['left'] > self.best['right']:
					# Interpolate
					better_neighbor = self.best['left']
					chosen_size = 'left'
				else:
					better_neighbor =  self.best['right']
					chosen_size = 'right'

				c_stats = self.stats[_idx(self.best['center'])]
				n_stats = self.stats[_idx(better_neighbor)]
				new_x = (c_stats * self.best['center'] + n_stats * better_neighbor) / (c_stats + n_stats)


			new_y = self.eval_fun(new_x)

			# Update best
			if chosen_side == 'left None':
				if new_y > self.stats[_idx(self.best['center'])]:
					self.best = {'center': new_x, 'left': None, 'right': self.best['center']}

					# Discount prev best
					self.best['right'] *= self.discount
				else:
					self.best['left'] = new_x

					# Discount prev best
					self.best['center'] *= self.discount

			elif chosen_side == 'right None':
				if new_y > self.stats[_idx(self.best['center'])]:
					self.best = {'center': new_x, 'left': self.best['center'], 'right': None}

					# Discount prev best
					self.best['left'] *= self.discount
				else:
					self.best['right'] = new_x

					# Discount prev best
					self.best['center'] *= self.discount

			elif chosen_side == 'left':
				if new_y > self.stats[_idx(self.best['center'])]:
					self.best = {'center': new_x, 'left': self.best['left'], 'right': self.best['right']}

					# Discount prev best
					self.best['left'] *= self.discount
					self.best['right'] *= self.discount
				else:
					self.best['right'] = new_x

					# Discount prev best
					self.best['center'] *= self.discount

			elif chosen_side == 'right':
				if new_y > self.stats[_idx(self.best['center'])]:
					self.best = {'center': new_x, 'left': self.best['center'], 'right': None}

					# Discount prev best
					self.best['left'] *= self.discount
				else:
					self.best['right'] = new_x

					# Discount prev best
					self.best['center'] *= self.discount

				


			self._update_memory(new_x, new_y)

		return new_x, new_y