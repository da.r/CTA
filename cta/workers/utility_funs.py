import functools
import numpy as np

seed = 500


def u_perfect(alpha=1):
    '''
    Creates a utility function for a worker-task pair with perfect performance.
    
    Args:
        alpha (float): determines the utility returned by perfect solution
    '''
    return lambda c: alpha * np.ones_like(c)


def u_reject():
    '''
    Creates a utility function for a worker-task pair which rejects tasks.
    '''
    return lambda c: np.zeros_like(c)


def u_logistic_cost(alpha=1, beta=1, gamma=7):
    '''
    Creates a Logistic cost utility function.

    Args:
        alpha (float): represents maximum utility
        beta (float): specifies convergence speed
        gamma (float): compensation level at which half of the maximum utility is achieved when `beta` = 1
    '''
    return lambda c: (alpha / (1 + (np.exp(-(beta * c) + gamma))))


def u_bounded_logistic_cost(min_bound=1, alpha=1, beta=1, gamma=7):
    '''
    Creates a bounded logistic cost utility function.

    Args:
        min_bound (float): compensation level under which the worker does not accept task, i.e. utility is zero
        alpha (float): represents maximum utility
        beta (float): specifies convergence speed
        gamma (float): compensation level at which half of the maximum utility is achieved when `beta` = 1
    '''
    def f(c):
        tmp = u_logistic_cost(alpha=alpha, beta=beta, gamma=gamma)(c)
        if isinstance(tmp, np.ndarray):
            tmp[c < min_bound] = 0
        else:
            tmp = 0 if c < min_bound else tmp
        return tmp
    return f


def _u_private_cost(min_c, c):
    '''
    Auxiliary function used in generation of the fixed private cost.

    Args:
        min_c (float): smallest compensation level at which worker accepts a task
        c (float, np.ndarray): given compensation
    '''
    tmp = (c >=  min_c)
    if isinstance(tmp, np.ndarray):
        tmp[c <= min_c] = 0
    else:
        tmp = 0 if c <= min_c else tmp
    return tmp
def u_fixed_private_cost(min_bound=5, max_bound=30, random_state=seed):
    '''
    Creates a fixed private cost utility function.

    Args:
        min_bound (float): lowest possible value of the smallest acceptable compensation level
        max_bound (float): highest possible value of the smallest acceptable compensation level
        random_state (int): seed for the random number generator
    '''
    rng = np.random.default_rng(random_state)
    fixed_min = rng.uniform(min_bound, max_bound)
    return functools.partial(_u_private_cost, fixed_min)

                             
def u_step_cost(steps=np.column_stack((np.arange(9, 20), np.arange(0.0, 1.1, 0.1)))):
    '''
    Creates a utility function in which at some compensation levels the output suddenly increases and stays constant
    until the next step.

    Args:
        steps (np.ndaarray): compensation-value pairs at which the function output jumps. Array should contain start
            and end.
    '''
    def f(c):
        # For everything after the last step len(steps) returned
        step = np.digitize(c, steps[:-1, 0])
        return steps[step, 1]
    return f
            


def u_spammer_hammer(reference=5, alpha=0.5, random_state=seed):
    '''
    Creates a utility function equivalent to the spammer-hammer model.

    Args:
        reference (float): compensation level under which the worker does not accept tasks
        alpha (float): probability of behaving as a spammer (i.e. returned value is random)
        random_state (int): seed for the random number generator
    '''
    def f(c):
        rng = np.random.default_rng(random_state)
        if isinstance(c, np.ndarray):
            tmp = rng.uniform(size=c.size)
            tmp_alpha_check = rng.uniform(size=c.size)

            tmp[c <= reference] = 0
            tmp[(c > reference) & (tmp_alpha_check >= alpha)] = 1

            return tmp
        else:
            if c > reference:
                if rng.uniform() < alpha:
                    return rng.uniform()
                else:
                    return 1
            else:
                return 0

    return f