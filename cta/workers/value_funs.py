import numpy as np
import functools

from workers.utility_funs import *

random_state = 500

def reject():
    '''
    Value function that corresponds to a worker rejecting a task. Returns 0 no matter what compensation.
    '''
    return lambda c: np.zeros_like(c)


def logistic_cost(alpha=1, beta=1, gamma=7, compensation_fun=lambda c: c):
    '''
    Creates a logistic cost value function by combining the corresponding utility function with the given compensation
    function.

    Args:
        alpha (float): represents maximum utility
        beta (float): specifies convergence speed
        gamma (float): compensation level at which half of the maximum utility is achieved when `beta` = 1
        compensation_fun (function): compensation function which weighs the importance of compensation
    '''
    def f(c):
        weighted_c = compensation_fun(c)
        tmp = u_logistic_cost(alpha, beta, gamma)(c)
        if isinstance(tmp, np.ndarray):
            tmp /= weighted_c
            tmp[c <= 0] = 0
            tmp[tmp == np.inf] = 0
            tmp[tmp == np.nan] = 0
        else:
            tmp = 0 if weighted_c == 0 else tmp / weighted_c
        return tmp
    return f


def _private_cost(min_c, c, compensation_fun=lambda c: c):
    '''
    Auxiliary function used in generation of the private and fixed private cost.

    Args:
        min_c (float): smallest compensation level at which worker accepts a task
        c (float, np.ndarray): given compensation
        compensation_fun (function): compensation function which weighs the importance of compensation
    '''
    weighted_c = compensation_fun(c)
    tmp = np.array((c >=  min_c), dtype='float')
    if isinstance(tmp, np.ndarray):
        tmp /= weighted_c
        tmp[c <= 0] = 0
        tmp[tmp == np.inf] = 0
        tmp[tmp == np.nan] = 0
    else:
        tmp = 0 if weighted_c == 0 else tmp / weighted_c
    return tmp

rng = np.random.default_rng(seed=random_state)
def private_cost(min_bound=5.5, max_bound=30, compensation_fun=lambda c: c, rng=rng):
    '''
    Creates a private cost value function that is not deterministic. Every time it is called, another compensation
    level between `min_bound` and `max_bound` is considered to be the smallest acceptable compensation level.

    Args:
        min_bound (float): lowest possible smallest compensation level at which worker accepts a task
        max_bound (float): highest possible smallest compensation level at which worker accepts a task
        compensation_fun (function): compensation function which weighs the importance of compensation
        rng (np.random._generator.Generator): random number generator
    '''
    return lambda c: _private_cost(rng.uniform(min_bound, max_bound), c, compensation_fun=compensation_fun)


def fixed_private_cost(min_bound=5.5, max_bound=30, compensation_fun=lambda c: c, random_state=500):
    '''
    Creates a fixed private cost value function.

    Args:
        min_bound (float): lowest possible smallest compensation level at which worker accepts a task
        max_bound (float): highest possible smallest compensation level at which worker accepts a task
        compensation_fun (function): compensation function which weighs the importance of compensation
        random_state (int): seed for the random number generator
    '''
    rng = np.random.default_rng(seed=random_state)
    fixed_min = rng.uniform(min_bound, max_bound)
#     return lambda c: (c >= rng.uniform(min_bound, max_bound)) / c
    return functools.partial(_private_cost, fixed_min)


def bounded_logistic_cost(min_bound=1, alpha=1, beta=1, gamma=7, compensation_fun=lambda c: c):
    '''
    Creates a bounded logistic cost value function.

    Args:
        min_bound (float): lowest possible smallest compensation level at which worker accepts a task
        alpha (float): represents maximum utility
        beta (float): specifies convergence speed
        gamma (float): compensation level at which half of the maximum utility is achieved when `beta` = 1
        compensation_fun (function): compensation function which weighs the importance of compensation
    '''
    def f(c):
        tmp = logistic_cost(alpha=alpha, beta=beta, gamma=gamma, compensation_fun=compensation_fun)(c)
        if isinstance(tmp, np.ndarray):
            tmp[c < min_bound] = 0
        else:
            tmp = 0 if c < min_bound else tmp
        return tmp
    return f


def step_cost(steps=np.column_stack((np.arange(9, 20), np.arange(0.0, 1.1, 0.1))), compensation_fun=lambda c: c):
    '''
    Creates a value function which uses the utility step function.

    Args:
        steps (np.ndaarray): compensation-value pairs at which the function output jumps. Given array should contain start
            and end.
        compensation_fun (function): determines the influence of issued compensation
    '''
    def f(c):
        # For everything after the last step len(steps) returned
        tmp = u_step_cost(steps)(c) / c
        if isinstance(tmp, np.ndarray):
            tmp[c <= 0] = 0
        else:
            tmp =  0 if c <= 0 else tmp
        return tmp
    return f


def spammer_hammer(reference=5, alpha=0.5, compensation_fun=lambda c: c, random_state=seed):
    '''
    Creates a value function corresponding to the spammer-hammer model.

    Args:
        reference (float): compensation level under which the worker does not accept tasks
        alpha (float): probability of behaving as a spammer (i.e. returned utility is random)
        compensation_fun (function): compensation function which weighs the importance of compensation
        random_state (int): seed for the random number generator
    '''
    def f(c):
        modified_c = compensation_fun(c)
        return u_spammer_hammer(reference, alpha, seed)(c) / modified_c if modified_c > 0 else 0
    return f