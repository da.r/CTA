import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import minimize_scalar

from workers.value_funs import *


def _generate_worker_fun(value_fun, time_change_fun, y_noise_fun=None):
    '''Needed as a separate auxiliary function because of the lambda scoping rules in python'''
    if y_noise_fun is not None:
        return lambda k, x: y_noise_fun(value_fun(time_change_fun(k, x)))
    else:
        return lambda k, x: value_fun(time_change_fun(k, x))


def compute_optimal_comp_over_rounds(out, num_rounds, worker_funs, domain, return_values=False, always_k=None, 
                                     verbose=False, v_interval=10):
    '''
    Computes in-place optimal commpensation over the given number of rounds for the specified worker functions.
    
    Args:
        return_value (bool): whether the set of optimal compensations and values should be returned
        always_k (bool): computes optimal compensation-value pairs only once. Can be used in a static scenario when 
            worker_funs` do not change over time.
    '''
    if always_k is not None:
        if return_values:
            tmp_C, tmp_y = compute_optimal_comp(worker_funs, domain, always_k, return_values=return_values, quiet=True)
        else:
            tmp_C = compute_optimal_comp(worker_funs, domain, always_k, return_values=return_values, quiet=True)
    for k in range(num_rounds):
        if always_k is not None:
            if return_values:
                out[0], out[1] = tmp_C, tmp_y
            else:
                out[k] = tmp_C
        else:
            if return_values:
                out[0][k], out[1][k] = compute_optimal_comp(worker_funs, domain, k, return_values=return_values, quiet=True)
            else:
                out[k] = compute_optimal_comp(worker_funs, domain, k, return_values=return_values, quiet=True) 
            
        if verbose and (k % v_interval == 0):
            print(f'Round: {k:7}/{num_rounds}', end='\r')
    
    print(80*'-')
    print(f'Finished computing optimal compensation over {num_rounds} rounds!')


def compute_optimal_comp(W, domain, k=0, average_tasks=False, average_workers=False, return_values=False, verbose=False, v_interval=10, quiet=False):
    '''
    Computes at the specified round the optimal compensations for a given set of value functions. 
    
    Args:
        average_tasks (bool):  if True, only an average compensation over all optimal compensations for a worker is stored. 
        average_worker (bool):  if True, only an average compensation over all optimal compensations for a task is stored. 
    '''
    opt = np.empty_like(W)

    if return_values:
        opt_vals = np.empty_like(W)
    for i in range(W.shape[0]):
        for j in range(W.shape[1]):
            res = minimize_scalar(lambda x: -W[i, j](k, x), bounds=domain, method='bounded')
            opt[i, j] = res.x

            if return_values:
                opt_vals[i, j] = -res.fun
        
        if verbose and (i % v_interval == 0):
            print(f'Current worker: {i:7}/{W.shape[0]}', end='\r')
    
    if not quiet:
        print(80*'-')
        print(f'Finished computing optimal compensations for round {k}!')
        print(80*'-')
    
    if average_tasks:
        opt = np.average(opt, axis=1)
    if average_workers:
        opt = np.average(opt, axis=0)
    
    if return_values:
        return opt, opt_vals
    else:
        return opt


def generate_workers(N, M, 
                     funs=(logistic_cost, bounded_logistic_cost, fixed_private_cost, ), 
                     param_bounds=(((0.1, 3), (0.1, 1), (3, 25)), ((5, 29.5), (0.1, 3), (0.1, 1), (3, 25)), ((3, 5), (10,20)), ),
                     time_change_fun=lambda k, x: x + 0.1*k, time_change_prob=1, without_noise_fun=None,
                     y_noise_fun=None, verbose=False, v_interval=5, out=None, random_state=500):
    '''
    Generates worker-task value functions. Every generated value function takes two parameters: `k` (round)
    parameter and `x` a compensation level.
    
    Args:
        N (int): number of workers
        M (int): number of tasks
        funs ([function]): list of value functions from which workers are generated
        param_bounds ([tuple]): list of tuples containing bounds for the parameters of the value function creator 
            functions specified in `funs`. Parameter bounds can determine the similarity among worker population.
        time_change_fun (function): determines how the function shifts on the x-axis. Model change over rounds if it
            depends on round `k`.
        time_change_prob (float): defines a probability that the generated value function is shifted. Usually 
            corresponds to a percentage of all worker-task pair value functions are shifted
        without_noise_fun (function): should not be `None` if actual functions not affected by noise should be returned
        y_noise_fun (function): evaluation function that contains evaluation noise
        verbose (bool): if true, progress information printed out
        v_interval (int): at every `v_interval` information about the current progress is printed out
        out (np.ndarray): np.array that takes the generated value functions (alternative to assignment)
        random_state (int): seed for the random number generator
    '''
    
    rng = np.random.default_rng(random_state)
    no_time_change = lambda k, x: x

    workers = np.empty((N, M), dtype=object)

    if without_noise_fun is not None:
        workers_no_noise = np.empty((N, M), dtype=object)

    for i in range(N):
        f_type = rng.choice(len(funs))

        f = funs[f_type]
        for j in range(M):
            gen_params = [rng.uniform(*bound) for bound in param_bounds[f_type]]
            f_instance = f(*gen_params)

            if rng.uniform() < time_change_prob:
                used_time_change_fun = time_change_fun
            else:
                used_time_change_fun = no_time_change

            workers[i, j] = _generate_worker_fun(f_instance, used_time_change_fun, y_noise_fun)
            if without_noise_fun is not None:
                workers_no_noise[i, j] = _generate_worker_fun(f_instance, used_time_change_fun)

        if i % 20 == 0:
            print(f'Progress: {i+1:5}/{N}', end='\r')

        if verbose and (i % v_interval == 0):
            print(f'Current worker: {i:7}/{N}', end='\r')
        
    print(80*'-')
    print(f'Finished generating {N} workers with {M} tasks!')
    print(80*'-')

    if out is not None:
        out = workers
    else:
        if without_noise_fun is not None:
            return workers, workers_no_noise
        return workers