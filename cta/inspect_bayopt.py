import scipy as sp

from vfo import BayOpt, rbf
from visual.visualization import *
from acquisition import *
from environment_manager import *

import timeit

def learn(bo, rounds=5, domain=(5, 30), verbose=True, v_interval=100):
    '''
    BayOpt instance only learns, it is not plotted.
    '''
    idx_best_seen = None
    explore = True

    opt = sp.optimize.minimize_scalar(lambda x: -bo.actual_fun(0, x), method='bounded', bounds=domain)
    y_opt = bo.actual_fun(0, opt.x)
    err = []
    max_af = []

    # Initial max max AF value
    f_af = lambda x: -bo.AF(x, np.max(bo.assigned_y), bo.mu, bo.sigma_sq)
    res = sp.optimize.minimize_scalar(f_af, method='bounded', bounds=domain)
    max_af.append(-res.fun)

    err.append(y_opt - bo.assigned_y[-1])
    
    for k in range(1, rounds):

        opt = sp.optimize.minimize_scalar(lambda x: -bo.actual_fun(k, x), method='bounded', bounds=domain)
        y_opt = bo.actual_fun(k, opt.x)
        
        bo.update_posterior()

        new_x, new_obs, max_af_val = bo.next(k)


        max_af.append(new_obs)
        err.append(y_opt - bo.assigned_y[-1])

        if verbose and (k % v_interval == 0):
            print(f'Round: {k:7}/{rounds}', end='\r')

    print(80*'-')
    print('Finished!')
    return (bo.assigned_X, bo.assigned_y, err)


if __name__ == '__main__':
    # Settings
    plot = True
    plot_every_k_rounds = 1
    plot_refresh_time = 0.5

    rounds = 100
    rng = np.random.default_rng(500)
    domain = (5, 30)

    time_change_fun_no_noise = lambda k, x: x
    y_noise_fun = lambda x: x + rng.uniform(-0.1, 0.1)

    # Dynamic scenario
    # time_change_fun = lambda k, x: x - (k > 15)*3
    # Static scenario
    time_change_fun = lambda k, x: x

    # acquisition_function = create_biased_AF(comp_weigh_fun=lambda x: np.sqrt(x), AF=UCB)
    acquisition_function = UCB

    kernel = 1 * kernels.RBF(3) +  kernels.WhiteKernel(noise_level=0.01)
    # kernel = 1 * kernels.Matern(nu=0.5, length_scale=10) + kernels.WhiteKernel(0.01)
    # kernel = 1 * kernels.Matern(nu=0.5, length_scale=10) + kernels.WhiteKernel(0.01)\
    #                 + 1 * kernels.RBF(5, ((0.00001, 10), )) + kernels.WhiteKernel(0.01)

    W, W_no_noise = generate_workers(1, 1, funs=(bounded_logistic_cost, ), param_bounds=(((7, 10), (0.5, 5)), (1, 20)),
                         verbose=True, time_change_fun=time_change_fun, y_noise_fun=y_noise_fun, without_noise_fun=time_change_fun_no_noise)

    initial_point = (domain[1] - domain[0]) / 2
    bo = BayOpt([initial_point], domain, W_no_noise[0, 0], eval_fun=W[0, 0], mu_prior=lambda X: 0.0 * np.ones_like(X), 
                 kernel=kernel, noise=0.05, discretization=2, win_size=50, stats_memory_size=25, AF=acquisition_function,
                 loc_expl_rate=0.4, loc_expl=0.8, loc_expl_min=0.1, loc_expl_max=1, glob_expl_rate=0.5,
                 glob_expl=0.5, glob_expl_min=0.3, glob_expl_max=1, opt_pt_mean_fraction=2, opt_pt_std_fraction=1, 
                 opt_restarts=5, estimate_hyperparams=False, hyperparam_lr=0.2, reset_hyperparams=False, use_smw=False,
                 random_state=500)

    if plot:
        wr_X, wr_y = plot_learning(bo, rounds=rounds, domain=domain, plot_interval=plot_every_k_rounds, refresh_time=plot_refresh_time)
        print(f'Run finished:')
        print(f'\tCompensation issued after {rounds} rounds: {sum(wr_X):.2f}')
        print(f'\tValue after {rounds} rounds: {sum(wr_y):.2f}')
    else:
        timer_start = timeit.default_timer()
        wr_X, wr_y, err = learn(bo, rounds=rounds, domain=(5, 30))
        timer_end = timeit.default_timer()
        print(f'Run finished: \n\tTime: {timer_end - timer_start} s')
        print(f'\tCompensation issued after {rounds} rounds: {sum(wr_X):.2f}')
        print(f'\tValue after {rounds} rounds: {sum(wr_y):.2f}')
        print(f'\tOverall error {rounds} rounds: {sum(err):.2f}')

