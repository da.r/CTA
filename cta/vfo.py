import numpy as np

from scipy.optimize import minimize
from scipy.stats import truncnorm
from scipy.linalg import cho_factor, cho_solve

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process import kernels
from sklearn.base import clone

from math import ceil
from operator import itemgetter

from acquisition import EI


# Auxiliary functions
def rbf(X1, X2=None, nu=1, l=3, noise=1):
    '''
    RBF kernel function. If the input is multidimensional it returns kernel matrix.

    Args:
        X1 (scalar or np.ndarray): input scalar or vector
        X2 (scalar or np.ndarray, optional): input scalar or vector. If it is not set X2=X1
        nu (float, optional): scaling parameter
        l (float, optional): length parameter
        noise (float, optional): noise parameter
    '''
    # Needed because sklearn WhiteKernel doesn't compute anything with 2 arguments
    if X2 is None:
        X2 = X1
    # assert len(X1.shape) == 2 and len(X2.shape) == 2
    # X1 = X1[:,np.newaxis]
    X2 = X2.T

    # For 2 N-dim transpose doesn't change anything bcs of symmetry
    return (nu**2) * np.exp(-np.square(X1 - X2)/(2 * (l**2))) + noise * np.eye(X1.shape[0], X2.shape[1])


def _inv2x2(A):
    '''
    Inverts given matrix in-place.
    
    Args:
        A (np.ndarray): matrix to invert
    '''
    a = (1/(A[0, 0]*A[1, 1] - A[0, 1]*A[1, 0]))
    A[0, 0], A[0, 1], A[1, 0], A[1, 1] = A[1, 1], -A[0, 1], -A[1, 0], A[0, 0]
    A *=a


def smw(A_inv, U, V, out=None):
    '''
    Inverts a matrix using Sherman–Morrison–Woodbury formula. If `out` is not defined 
    inversion is done in-place.

    Args:
        A_inv ((N x N) np.ndarray): inverse calculated in the  previous step
        U ((N x 1) np.ndarray): column vector
        V ((1 x N) np.ndarray): row vector
    '''
    # assert (A_inv.shape[0] == A_inv.shape[1]), 'A should be square matrix N x N'
    # assert ((A_inv.shape[0], 2) == U.shape == V.T.shape), f'U and V.T should be column vectors N x 2. Seen U {U.shape}, V {V.T.shape}'
#     assert (1 + v.T @ A_inv @ u != 0), 'Formula condition that the calculated value represents the inverse'
    
    if out is not None:
        A_inv = out
    
    t1 = (A_inv @ U)
    t2 = np.identity(2) + V @ t1
    _inv2x2(t2)

    t3 = V @ A_inv
    
    if out is not None:
        A_inv -= t1 @ t2 @ t3
    else:
        return A_inv - t1 @ t2 @ t3
    


class BayOpt:
    def __init__(self, prior_X, domain, actual_fun, eval_fun, mu_prior=lambda X: 0.0 * np.ones_like(X), 
                 kernel=kernels.RBF(), noise=0.05, discretization=2, win_size=50, stats_memory_size=50, AF=EI, 
                 loc_expl_rate=0.01, loc_expl_jump=0.5, loc_expl=1.0, loc_expl_min=0.001, loc_expl_max=1, glob_expl_rate=0.2, 
                 glob_expl_jump=0.5, glob_expl=0.5, glob_expl_min=0.3, glob_expl_max=0.8, opt_pt_mean_fraction=8, opt_pt_std_fraction=8, 
                 opt_restarts=5, estimate_hyperparams=False, hyperparam_lr=0.001, reset_hyperparams=False, use_smw=False,
                 random_state=500):
        '''

        If `use_smw` is set to True, kernel ignores the sliding window because inverse from the previous step cannot be reused. 
        Instead of memory, all of the assigned data points are used.
        Args:
            prior_X (np.ndarray): points at which prior observations should be made
            domain (tuple): domain of the objective function
            actual_fun (function): function without noise
            eval_fun (function): represents evaluations, different from `actual_fun` because it can incorporate different 
                types of noise (e.g. noise can depend on the size of the input)
            mu_prior (function, optional): mean function of Gaussian process; zero mean by default
            kernel (function, optional): kernel or covariance function of Gaussian process
            noise (float, optional): hyperparameter which represents prior belief on the variance of evaluation noise
            discretization (int, optional): distance between two neighboring points is `10^(discretization)`; higher 
                values increase the need for space significantly
            win_size (int, optional): size of the sliding window
            stats_memory_size (int, optional): number of evaluations to consider when computing the average for discretized points
            AF (function, optional): acquisition function
            loc_expl_max (float): Should be in [0, 1].
        '''        
        # Efficiency improvement possible by using dictionaries as hash table
        self.memory_X = list(np.round(prior_X, discretization))
        self.memory_y = [actual_fun(0, x) for x in self.memory_X]

        self.assigned_X = self.memory_X.copy()
        self.assigned_y = self.memory_y.copy()

        self.discretization = discretization

        domain_size = domain[1] - domain[0]
        self.stats_avg = {np.round(domain[0]+((10**(-self.discretization))*c), self.discretization):0 for c in range(np.round((10**self.discretization) * domain_size + 1))}
        self.stats_times_seen = self.stats_avg.copy()
        self.stats_memory_size = stats_memory_size

        self.K_inv = None
        
        self.mu_prior = mu_prior
        self.K = kernel
        self.K_init = clone(kernel)
        self.noise = noise
        
        self.mu = self.mu_prior
        self.sigma_sq = lambda x: self.K(x, x)
        
        self.actual_fun = actual_fun
        self.eval_fun = eval_fun

        self.AF = AF
        self.use_smw = use_smw
        self.estimate_hyperparams = estimate_hyperparams
        self.win_size = win_size
        self.domain = domain

        self.hyperparam_lr = hyperparam_lr

        self.explore = True

        self.a, self.b, self._mean, self._std = self._create_initial_opt_pt_settings(domain, opt_pt_mean_fraction, 
                                                                                       opt_pt_std_fraction)
        self.opt_restarts = opt_restarts

        self.max_af_val = 0
        self.reset_hyperparams = reset_hyperparams

        # self.prev_seen = prior_X[-1]
        self.prev_assigned_with_neighboorhood_approach = False
        self.prev_assigned_with_global_approach = False


        self.rng = np.random.default_rng(random_state)
        
        if estimate_hyperparams:
            self.gp = GaussianProcessRegressor(self.K)
            self.gp.kernel_ = self.K

        # For local exploration)
        self.loc_expl_rate = loc_expl_rate
        self.loc_expl_jump = loc_expl_jump
        self.loc_expl_init = loc_expl
        self.loc_expl = loc_expl
        self.loc_expl_min = loc_expl_min
        self.loc_expl_max = loc_expl_max

        # For global exploration)
        self.glob_expl_rate = glob_expl_rate
        self.glob_expl_jump = glob_expl_jump
        self.glob_expl_init = glob_expl
        self.glob_expl = glob_expl
        self.glob_expl_min = glob_expl_min
        self.glob_expl_max = glob_expl_max

        self.rounds_at_drift_detection = []

        # Auxiliary variables
        self.prev_max_idx = np.argmax(self.memory_y)

        
    def _create_initial_opt_pt_settings(self, domain, mean_fraction=2, std_fraction=2):
        '''
        Auxiliary method that creates parameters for a truncated normal distribution which generates a starting point
        of acquisition function optimization.

        Args:
            domain (tuple): contains start and end point of domain
            mean_fraction (float): decides the position of the mean of the created distribution.
                value > 2: mean between lower bound and the middle of domain
                value = 2: mean in the middle of domain
                1 < value < 2: mean nearer to the upper bound
            std_fraction (float): determines the uncertainty (the lower the value the higher the uncertainty)

        Returns:
            (float, float, float/int, float/int): Returns parameters needed for the generation of initial point.
        '''
        _dom_size = domain[1]-domain[0]
        _mean = domain[0] + (_dom_size / mean_fraction)
        _std = _dom_size / std_fraction

        a = (domain[0] - _mean) / _std
        b = (domain[1] - _mean) / _std

        return a, b, _mean, _std


    def generate_init_pt(self):
        '''
        Method that generates an initial point for acquisition function maximization.

        Returns:
            (float): x in domain
        '''
        return truncnorm.rvs(self.a, self.b, loc=self._mean, scale=self._std)

    def _maximize_af(self):
        '''
        Auxiliary method that maximizes the acquisition function. Repeats local maximization `opt_restart` with an 
        initial point created by the `generate_init pt` method.

        Returns:
            (x: float, y: float): tuple containing optimal x and y = f(x)
        '''
        xs = []
        ys = []
        y_max = np.max(self.memory_y)
        for i in range(self.opt_restarts):
            res = minimize(lambda x: -self.AF(x, y_max, self.mu, self.sigma_sq), x0=self.generate_init_pt(), bounds=(self.domain, ), tol=1e-5)
            xs.append(res.x[0])
            ys.append(-res.fun)

        opt_idx = np.argmax(ys)

        return xs[opt_idx], ys[opt_idx]


    def _reset_kernel(self):
        '''
        Auxiliary function which resets the kernel and points kept in memory. Only the last point is kept. Usually
        called when hyperparameters overflow because hyperparameter learning rate `hyperparam_lr` was set too high. 
        '''
        if self.estimate_hyperparams:
            self.gp.kernel_.theta = self.K_init.theta

        self.K = clone(self.K_init)

        self.memory_X = self.memory_X[-1:]
        self.memory_y = self.memory_y[-1:]

        self.mu = self.mu_prior
        self.sigma_sq = lambda x: self.K(x, x)


    def _reset_explore(self, prev_x, prev_y):
        '''
        Resets exploration structures: global and local exploration parameters and stats.

        Args:
            prev_x (float): previously seen compensation x
            prev_y (float): previously seen observation y
        '''
        self.glob_expl = self.glob_expl_init
        self.loc_expl = self.loc_expl_init

        self.stats_avg = {np.round(domain[0]+((10**(-self.discretization))*c), self.discretization):0 for c in range(np.round((10**self.discretization) * domain_size + 1))}
        self.stats_times_seen = self.stats_avg.copy()

        # Keep a stats on a last seen subset of the window size
        to_keep = min(int(0.1 * self.win_size), len(self.assigned_X)-1)
        for i in range(to_keep, -1, -1):
            x = np.round(self.assigned_X[-i], self.discretization)
            obs = self.assigned_y[-i]
            self.stats_times_seen[x] += 1 if self.stats_times_seen[x] < self.stats_memory_size else 0
            self.stats_avg[x] = ((self.stats_times_seen[x] - 1) * self.stats_avg[x] + obs) / self.stats_times_seen[x]

        # Alternative: only remember the last point
        # self.stats_times_seen[prev_x] = 1
        # self.stats_avg[prev_x] = prev_y


    def next(self, k, given_c=None):
        '''
        Chooses the next point and evaluates the function.

        Args:
            k (int): current round
            given_c (float, optional): compensation to be issued

        Returns:
            (x: float, y: float, max_af_value: float): tuple containing `x` issued compensation, `y` observation and
                current maximum value of the acquisition function (which was potentially changed due to 'waiting'
                time)
        '''

        if self.explore and given_c is None:

            # Automatically search for the best rate and size of local explore
            if self.prev_assigned_with_neighboorhood_approach or self.prev_assigned_with_global_approach:
                y_prev_seen = self.assigned_y[-1]
                X_prev_seen = self.assigned_X[-1]
                
                idx_check = np.argmax(self.memory_y)
                # assert self.prev_max_idx == idx_check, f'{self.prev_max_idx} == {idx_check}'
                y_max_memory = self.memory_y[self.prev_max_idx]
                X_max_memory = self.memory_X[self.prev_max_idx]

                # Previously seen is always smaller (due to checking only smaller compensation)
                x_diff = X_max_memory - X_prev_seen

                # The larger relative difference the less we should explore
                y_diff = (y_max_memory - y_prev_seen) / y_max_memory if y_max_memory > 0 else 0
                # assert y_diff >= 0, f'y_diff should be >= 0, seen {y_diff, y_max_memory}'
                
                if y_diff < 0.001:
                    if self.prev_assigned_with_neighboorhood_approach:
                        # self.loc_expl *= 1 + self.loc_expl_rate
                        self.loc_expl += self.loc_expl_jump
                        # Alternative which considers relative distance too
                        # self.loc_expl *= 1 + self.loc_expl_rate * (y_prev_seen / y_max_memory)

                        if self.loc_expl > self.loc_expl_max:
                            self.loc_expl = self.loc_expl_max

                    # Can be considered as a detection of concept drift
                    elif self.prev_assigned_with_global_approach:
                        # Alternative reaction to concept drift detection which could be perform better for sudden 
                        # changes could be to reset stats and significantly increase exploration
                        # self.glob_expl *= 1 + 5*self.glob_expl_rate
                        # self._reset_explore(X_prev_seen, y_prev_seen)

                        # self.glob_expl *= 1 + self.glob_expl_rate
                        self.glob_expl += self.glob_expl_jump

                        self.rounds_at_drift_detection.append(k)

                        if self.glob_expl > self.glob_expl_max:
                            self.glob_expl = self.glob_expl_max

                elif y_diff < 0:
                    raise ValueError(f'Should be impossible, prev_seen cannot be larger than max_seen; max_seen: {y_max_memory}, prev_seen: {y_prev_seen}')

                else:
                    if self.prev_assigned_with_neighboorhood_approach:
                        self.loc_expl *= 1 - self.loc_expl_rate
                        
                        if self.loc_expl < self.loc_expl_min:
                            self.loc_expl = self.loc_expl_min
                        
                    elif self.prev_assigned_with_global_approach:
                        self.glob_expl *= 1 - self.glob_expl_rate
                        
                        if self.glob_expl < self.glob_expl_min:
                            self.glob_expl = self.glob_expl_min


            # Globally explore
            if self.rng.uniform() < self.glob_expl:
                next_x, self.max_af_val = self._maximize_af()

                self.prev_assigned_with_neighboorhood_approach = False
                self.prev_assigned_with_global_approach = True

            # Locally explore or exploit
            else:
                X_max_stats = max(self.stats_avg, key=self.stats_avg.get)
                y_max_memory = max(self.stats_avg.values())

                if self.rng.uniform() < self.loc_expl:
                    # One assumption, if simple micro-tasks (but not resources) are crowdsourced, is to
                    # only locally explore towards the area of lower compensation because if tasks are simple, the
                    # increased effort has low influence on the solution quality.
                    next_x = X_max_stats + self.rng.uniform(-2*self.loc_expl, 2*self.loc_expl)

                    self.prev_assigned_with_neighboorhood_approach = True
                    self.prev_assigned_with_global_approach = False
                else:
                    next_x = X_max_stats
                    self.prev_assigned_with_neighboorhood_approach = False
                    self.prev_assigned_with_global_approach = False

                    # Exploits, but the compensation changed. Implies drift detection.
                    prev_x = self.assigned_X[-1]
                    prev_y = self.assigned_y[-1]
                    if next_x != prev_x \
                        and self.stats_times_seen[prev_x] - self.stats_times_seen[next_x] > (0.1 * self.stats_memory_size) \
                        and np.abs(next_x - prev_x) < 2 * self.loc_expl_max:

                        # Alternative to reset the exploration parameters
                        # self._reset_explore(prev_x, prev_y)
                        
                        self.glob_expl *= 1 + self.glob_expl_rate
                        self.loc_expl *= 1 + self.loc_expl_rate

                        self.rounds_at_drift_detection.append(k)


        else:
            next_x = given_c


        # Discretize because it allows us to keep stats on the neighborhoods 
        # and cent is the smallest denomination.
        # Because of the local neighborhood discretization can be even less granular.
        # It helps to explore a bit more around the maximum if the function not smooth.
        next_x = np.round(next_x, self.discretization)

        if next_x < self.domain[0]:
            next_x = self.domain[0]
            # if next_x > self.domain[0] - 3 * self.loc_expl:
            #     next_x = self.domain[0]
            # else:
                # raise ValueError(f'NEXT_X < LOWER BOUND OF DOMAIN, `next_x` = {next_x}, \
                #     global: {self.prev_assigned_with_global_approach}, \
                #     local: {self.prev_assigned_with_neighboorhood_approach}')
        if next_x > self.domain[1]:
            next_x = self.domain[1]
            # if next_x < self.domain[1] + 3 * self.loc_expl:
            #     next_x = self.domain[1]
            # else:
            #     # raise ValueError(f'NEXT_X > UPPER BOUND OF DOMAIN `next_x` = {next_x} \
            #     #     global: {self.prev_assigned_with_global_approach}, \
            #     #     local: {self.prev_assigned_with_neighboorhood_approach}')
            

        new_x = next_x
            
        # When it jumps (because estimate_hyperparams is turned on), error appears here
        if new_x == 0:
            raise ZeroDivisionError(f'k = {k}, {next_x} in next x')        
            new_obs = 0
        else:
            new_obs = self.eval_fun(k, new_x)
            if new_obs > 1.5:
                raise Exception(f'{new_obs} = new_obs')
        
        self.explore = True

        # Value observations can be < 0 because of high levels of noise.
        # Consider 0 to be the lowest possible value.
        new_obs = 0 if new_obs < 0 else new_obs


        self.memory_X.append(new_x)
        self.memory_y.append(new_obs)

        # Allows to avoid repeated calling of argmax()
        if new_obs > self.memory_y[self.prev_max_idx]:
            self.prev_max_idx = len(self.memory_y) - 1
                
        # Adjust memory depending on window size
        if len(self.memory_X) > self.win_size:
            removed_x = np.round(self.memory_X.pop(0), self.discretization)
            removed_y = self.memory_y.pop(0)

            self.stats_times_seen[removed_x] -= 1 if self.stats_times_seen[removed_x] > 0 else 0
            if self.stats_times_seen[removed_x] == 0:
                self.stats_avg[removed_x] = 0
            else:
                self.stats_avg[removed_x] = (((self.stats_times_seen[removed_x] + 1) * self.stats_avg[removed_x]) - removed_y) / self.stats_times_seen[removed_x]

            self.prev_max_idx -= 1
            if self.prev_max_idx < 0:
                self.prev_max_idx = np.argmax(self.memory_y)

        
        # Shouldn't evaluate fun after memory_X or memory_y are updated before update_posterior
        self.assigned_X.append(new_x)
        self.assigned_y.append(new_obs)

        self.stats_times_seen[new_x] += 1 if self.stats_times_seen[new_x] < self.stats_memory_size else 0
        self.stats_avg[new_x] = ((self.stats_times_seen[new_x] - 1) * self.stats_avg[new_x] + new_obs) / self.stats_times_seen[new_x]

        return new_x, new_obs, self.max_af_val


    def update_posterior(self, use_smw=None, win_size=None):
        '''
        Updates gaussian process posterior. Accepts incremental updates with one new point. Either `use_smw` or the sliding window should be selected.

        Args:
            use_smw (bool, optional): if True Woodbury matrix identity is used to invert the matrix
            win_size (int, optional): sliding window size. If given, overwrites the value used on instance creation.
        '''
        
        if not self.explore:
            return

        if win_size is None:
            win_size = self.win_size
        if use_smw is None:
            use_smw = self.use_smw

        if use_smw:
            X = np.array(self.assigned_X)
            y = np.array(self.assigned_y)
        else:
            X = np.array(self.memory_X[-win_size:])
            y = np.array(self.memory_y[-win_size:])
        X = X.reshape(-1, 1)
        y = y.reshape(-1, 1)

        if X.size:
            # Iterative improvement 
            # Uses the fact we have K available with one point less.
            if use_smw:
                # 1st round
                if self.K_inv is None:
                    # self.K_inv = 1/self.K(X, X)
                    # There is a difference in values between K(X) and K(X, X)
                    self.K_inv = 1/(self.K(X) + np.diag(np.ones_like(X)*self.noise) + np.diag(np.ones_like(X)*1e-7))

                # 2nd round
                elif self.K_inv.size == 1:
                    cov = self.K(X[:1])
                    self.K_inv = np.identity(2)
                    self.K_inv[0, 1], self.K_inv[1, 0] = cov, cov
                    _inv2x2(self.K_inv)

                # nth round
                else:
                    U = np.zeros((X.size, 2))
                    
                    U[:-1, 1] = self.K(X[:-1], X[-1:]).ravel()
                    U[-1, 0] = 1

                    V = np.zeros((2, X.size))
                    V[0, :] = U[:,1] 
                    V[1, -1] = 1

                    # K(X) instead of K(X, X) because WhiteKernel doesn't work with K(X, X)
                    U[-1, 1] = self.K(X[-1]) - 1 + self.noise + 1e-7

                    self.K_inv = np.pad(self.K_inv, pad_width=((0,1), (0,1)))
                    self.K_inv[-1,-1] = 1

                    self.K_inv = smw(self.K_inv, U, V)
            
            else:
                if self.reset_hyperparams:
                    cur_K = self.K_init
                else:
                    cur_K = self.K

                self.K_inv = cho_solve(cho_factor(cur_K(X) + np.diag(np.ones_like(X)*self.noise) + np.diag(np.ones_like(X)*1e-7), lower=True), np.identity(X.shape[0]))
                

            # Inspired by the sklearn implementation,
            # thus sklearn kernel has to be used
            if self.estimate_hyperparams and self.explore:
                gp = self.gp

                gp.X_train_ = X
                gp.y_train_ = y

                # Do one step of gradient ascent on marginal log likelihood
                # if len(self.memory_X) > 1:
                alpha = self.K_inv @ y
                if self.reset_hyperparams:
                    self._reset_kernel(k)
                else:
                    _, K_grad = self.K(X, eval_gradient=True)

                # Analogous to the einsum trick from sklearn
                # Like np.trace(tmp @ K_grad)
                tmp = (alpha @ alpha.T - self.K_inv)[..., np.newaxis]
                t1 = np.einsum('ijk,jil->l', tmp, K_grad)

                grad = 0.5 * t1

                if self.reset_hyperparams:
                    gp.kernel_.theta = self.hyperparam_lr * grad
                else:
                    # Changes K too, because they point to the same object
                    abs_grad = np.abs(grad)

                    # Reset grad and memory if anomalous behavior (too large, or nan or inf)
                    if np.any((abs_grad > 1e3)):
                    # if np.any((grad == np.inf) | (grad == np.nan)):
                        gp.kernel_.theta = self.K_init.theta
                        self.memory_X = self.memory_X[-1:]
                        self.memory_y = self.memory_y[-1:]
                        self.prev_max_idx = 0
                    else:
                        gp.kernel_.theta += (self.hyperparam_lr / len(self.memory_X)) * grad
                
                
            def g(x):
                return self.mu_prior(x) + \
                                    ((self.K(x, X) @ self.K_inv) @ \
                                         (y - self.mu_prior(X)))
            self.mu = g

            def f(x):
                return self.K(x, x) - (self.K(x, X) @  self.K_inv) @ self.K(X, x)
            self.sigma_sq = f


        else:
            return self.mu_prior, lambda x: self.K(x, x)




class Zooming:
    '''
    Implementation of the zooming algorithm for value function optimization based on continuum-armed bandits model.
    
    writing:
        Zooming algorithm does not keep the neighborhood fixed, but instead adapts the size or the number of the neighborhood which it investigates. This allows it to 
        concentrate more closely on promising areas by increasing discretization granularity.

    Args:
        domain ((float, float)): tuple containing lower and upper bound of domain
        eval_fun (function): evaluation function which takes current round `k` and chosen compensation `c` as input, i.e. `eval_fun(k, c)`
        conf_importance (float): parameter which sets the importance of confidence ball. The higher the value, the more exploration.
        init_active (int): ; number of initially active neighborhoods with equal distances between them;  usually `10^x`
        neighborhoods (int): number of neighborhoods that can be investigated
    '''

    def __init__(self, domain, eval_fun, conf_importance=2, init_active=2, neighborhoods=20, L=1):
        # self.rng = np.random.default_rng(random_state)
        self.eval_fun = eval_fun

        self.domain_size = domain[1] - domain[0]
        self.domain = domain
        self.neighborhoods = neighborhoods
        
        # self.times_seen = {np.round(domain[0] + (x * (domain_size / neighborhoods)), 2):0 for x in range(neighborhoods)}
        # + 1 to account for domain bounds
        self.times_seen = np.zeros(neighborhoods + 1)
        self.confidence_ball = np.zeros(neighborhoods + 1)
        self.avg = np.zeros(neighborhoods + 1)
        self.index = np.zeros(neighborhoods + 1)

        self.active = np.array([x % int(neighborhoods / init_active) == 0 for x in range(neighborhoods + 1)])
        self.max_cover_num = ceil((neighborhoods / init_active) / 2)

        self.assigned_X = []
        self.assigned_y = []

        self.conf_importance = conf_importance
        self.L = L


    def _get_val(self, idx):
        return self.domain[0] + idx * np.round(self.domain_size / self.neighborhoods, 2)

    def next(self, k, given_c=None):

        self.confidence_ball = self.active * np.sqrt((2 * np.log(k)) / (self.times_seen + 1))

        self.index = self.active * (self.avg + self.conf_importance * self.confidence_ball)
        # self.index = self.active * (self.avg + 0.2 * self.confidence_ball)

        if given_c is not  None:
            return given_c, self.eval_fun(k, given_c)


        selected_idx = np.argmax(self.index)
        selected_val = self._get_val(selected_idx)

        new_obs = self.eval_fun(k, selected_val)
        # Value observations can be < 0 because of high levels of noise.
        # Consider 0 to be the lowest possible value.
        new_obs = 0 if new_obs < 0 else new_obs

        self.avg[selected_idx] = ((self.times_seen[selected_idx] * self.avg[selected_idx]) + new_obs) / (self.times_seen[selected_idx] + 1)

        self.times_seen[selected_idx] += 1

        # Ball decreases only on update, thus check change in cover
        new_ball = np.sqrt((2 * np.log(k)) / (self.times_seen[selected_idx] + 1))
        self.confidence_ball[selected_idx] = new_ball
        for i in range(self.max_cover_num, 0, -1):
            x_low = self._get_val(selected_idx - i)
            x_high = self._get_val(selected_idx + i)

            if self.L * (selected_val - x_low) > new_ball:
                self.active[selected_idx - i] = 1
            elif self.L * (x_high - selected_val) > new_ball:
                self.active[selected_idx + i] = 1
            else:
                break

        self.assigned_X.append(selected_val)
        self.assigned_y.append(new_obs)

        return selected_val, new_obs