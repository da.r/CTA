import numpy as np
import math
import timeit

from acquisition import EI
from assignment import CrowdTAs

class EXP3CrowdTAs(CrowdTAs):
	def __init__(self, num_workers, num_tasks, num_rounds, worker_funs, eval_funs, VFO_args,
				 keep_in_memory=None, TA_rule='mean', TA_rule_discount_factor=0.9, TA_rule_win=50,
				 domain=(5, 30), assure_iden_comp=True, wait_expl_rate=0.0001,
				 alg={'name': 'exp3'}, use_best_next=False, random_state=500):
		'''

		Only works with Bayesian optimization used as `VFO_method`.
		Args:
			worker_funs (np.array(function))
			alg (dictionary): contains algrithm name ('exp3', 'exp3elm' or 'exp3++') and if needed params
			use_best_next (bool): specifies whether to use the heuristic from CrowdTAs
		'''
		super().__init__(num_workers, num_tasks, num_rounds, worker_funs, eval_funs, VFO_args,
				 keep_in_memory=keep_in_memory, TA_rule=TA_rule, TA_rule_discount_factor=TA_rule_discount_factor, TA_rule_win=TA_rule_win,
				 domain=domain, assure_iden_comp=assure_iden_comp, wait_expl_rate=wait_expl_rate,
				 random_state=random_state)

		self.prev_eps = (1 / self.M) * np.ones((self.N, self.M)) 
		self.val_sum = np.zeros((self.N, self.M))
		self.probs = np.zeros((self.N, self.M))
		self.alg = alg
		self.use_best_next = use_best_next

		if alg['name'] == 'exp3elm':
			self.active_tasks = [[m for m in range(self.M)] for _ in range(self.N)]
			self.active_tasks_mask = np.ones((self.N, self.M))
			self.B = 4 * (math.e - 2) * (2 * np.log(self.M) + np.log(2 / alg['confidence']))
			self.V = np.zeros((self.N, self.M))
			self.prev_V = np.zeros((self.N, self.M))
		elif alg['name'] == 'exp3++':
			self.eta = np.zeros((self.N, self.M))
			self.ksi = np.zeros((self.N, self.M))

			self.emp_gap = np.zeros((self.N, self.M))

			self.loss_sum = np.zeros((self.N, self.M))
			self.prev_loss_sum = np.zeros((self.N, self.M))
		# elif alg['name'] == 'exp3++ge':
			# For gap estimation
			self.L_hat = np.zeros((self.N, self.M))
			self.prev_L_hat = np.zeros((self.N, self.M))

			# Constant fixes division by 0 in the first round
			self.N_seen = np.ones((self.N, self.M)) * 0.0001
			self.prev_N_seen = np.ones((self.N, self.M)) * 0.0001

			
	def exp3_assignment(self, n, k):

		eps = np.min([1/self.M, np.sqrt(np.log(self.M) / (self.M * k))])

		t1 = (1 - self.M * eps)
		t2 = np.exp(self.prev_eps[n] * self.val_sum[n,:])
		t3 = np.sum(t2)

		self.probs[n] = (t1 * (t2 / t3)) + eps

		self.t[n] = self.rng.choice(self.M, p=self.probs[n])
		self.prev_eps[n] = eps

		return t2 / t3


	def exp3elm_assignment(self, n, k):

		eps = np.min([1/self.M, np.sqrt(np.log(self.M) / (self.M * k))])


		t1 = (1 - len(self.active_tasks[n]) * eps)
		t2 = np.exp(self.prev_eps[n] * self.val_sum[n,:])
		t3 = np.sum(t2)

		# Reset probs because of changing number of considered arms
		self.probs[n] = np.zeros(self.M)
		self.probs[n] = (t1 * (t2 / t3)) + eps

		self.t[n] = self.rng.choice(self.M, p=self.probs[n])
		self.prev_eps[n] = eps


	def exp3pp_assignment(self, n, k):
		'''
		Eta and ksi set to parameters from Seldin_2014 which they called 'empirical EXP3' algorithm
		'''

		beta = 0.5 * np.sqrt(np.log(self.M) / (self.M * k))
		tmp = ((self.loss_sum[n] - self.loss_sum[n].min()) / k)

		self.emp_gap[n] = np.row_stack((np.ones(self.M), tmp)).max(axis=0)
		assert np.all(self.emp_gap[n] >= 0)

		self.eta[n] = beta
		self.ksi[n] = np.where(self.emp_gap[n] == 0, 0, np.log(k * self.emp_gap[n]**2) / (32 * k * self.emp_gap[n]**2))
		assert np.all(self.ksi[n] >= 0)

		preliminary_min = np.min([1/(2 * self.M), beta])
		arr_prel_min = np.array(self.M * [preliminary_min])
		eps = np.stack((arr_prel_min, self.ksi[n])).min(axis=0)
		assert len(eps) == self.M

		t1 = (1 - np.sum(eps))
		t2 = np.exp(-self.prev_loss_sum[n] * self.eta[n,:])
		t3 = np.sum(t2)

		self.probs[n] = (t1 * (t2 / t3)) + eps

		self.t[n] = self.rng.choice(self.M, p=self.probs[n])
		self.prev_eps[n] = eps

		# Can be used in gap estimation algo
		return t2 / t3


	def exp3pp_ge_assignment(self, n, k, p, alpha=3, beta=256):
		'''Default alpha and beta from [Seldin and Lugosi, 2017] paper
		
		According to proposition in the paper parameters should be: alpha >= 3 and beta >= 64 * (alpha + 1) >= 256
		'''

		# Round counting starts at 1 in task assignments
		if k < self.M + 1:
			self.t[n] = k - 1
		else:

			tmp1 = self.prev_L_hat[n] / self.prev_N_seen[n]
			tmp2 = np.sqrt((alpha * np.log(k * self.M**(1 / alpha))) / (2 * self.prev_N_seen[n]))

			ucb = np.min([np.ones(self.M), tmp1 + tmp2], axis=0)
			lcb = np.max([np.zeros(self.M), tmp1 - tmp2], axis=0)

			gap = np.max([np.zeros(self.M), lcb - ucb.min()], axis=0)
			assert np.all((gap >= 0) & (gap <= 1)), 'gap in [0, 1]'

			tmp_eps_1 = (1 / (2 * self.M)) * np.ones(self.M)
			tmp_eps_2 = (0.5 * np.sqrt(np.log(self.M) / (k * self.M))) * np.ones(self.M)
			xi = (beta * np.log(k)) / (k * gap**2)

			assert tmp_eps_1.shape == tmp_eps_2.shape == xi.shape, 'shapes should be the same'

			eps = np.min([tmp_eps_1, tmp_eps_2, xi], axis=0)

			self.probs[n] = (1 - np.sum(eps)) * p + eps


	def run(self, plot=None, num_rounds=None, AF=None, assure_iden_comp=True, verbose=False, v_interval=10, opt_assign=None, opt_comp=None):
		if num_rounds is not None:
			self.num_rounds = num_rounds
		if AF is not None:
			self.AF = AF
		
		# Compensation proposition
		self.C_p = np.zeros(self.N)

		if plot is not None:
			bo_plot = BayOptPlot(self.bo[0, 0], self.num_rounds, self.S_c[0], self.S_y[0], plot_interval=1)

		for k in range(1, self.num_rounds):

			for n in range(self.N):
				if self.alg['name'] == 'exp3':
					self.exp3_assignment(n, k)
				elif self.alg['name'] == 'exp3elm':
					self.exp3elm_assignment(n, k)
				elif self.alg['name'] == 'exp3++':
					p = self.exp3pp_assignment(n, k)
					self.exp3pp_ge_assignment(n, k, p)
				else:
					raise ValueError(f'Unknown algorithm name, name in alg parameter: "{self.alg["name"]}"')

				m = self.t[n]

				self._update_best_next(n, m)

				self.C_p[n] = self.best_next_c[n, m]

				self.m_chosen[m] += 1


			if self.must_assign_all:
				for m in range(self.M):

					if self.m_chosen[m] == 0:

						workers_sorted = np.argsort(self.best_next[:, m])[::-1]
						for i in workers_sorted:
							# Swap only if giving task has enough workers
							if self.m_chosen[self.t[i]] >= 2:
								self.m_chosen[self.t[i]] -= 1
								self.t[i] = m
								self.m_chosen[m] += 1
								break
						else:
							raise ValueError('There is not enough workers for the given number of tasks - "must_assign_all" cannot be True')

			else:
				pass


			if self.assure_iden_comp:
				self.assure_iden_comp_fun(self.C_p, k, out=self.C)
			else:
				self.C = self.C_p


			# Observe and store
			for i in range(self.N):

				m = self.t[i]
				self.times_assigned[i, m] += 1
				cur_bo = self.bo[i, m]

				if self.assure_iden_comp:
					idx_C = m
					# Already evaluated in assure_iden_comp -- evaluation = calling .next()
					# self.res_x[i], res_y, self.max_af_val[i] = cur_bo.next(k, given_c=C[m])
				else:
					idx_C = i
					# Let bayopt decide if equitable C not assured
					self.res_x[i], self.res_y[i], self.max_af_val[i] = cur_bo.next(k)
					self.C[i] = self.res_x[i]

				if plot is not None:
					if i == plot[0] and m == plot[1]:
						bo_plot.update_plot(cur_bo, k, self.res_x[i], self.res_y[i], self.max_af_val[i], self.S_c[k], self.S_y[k])

				# Update EXP3 specific variables
				if self.use_best_next:
					self.val_sum[i, m] = self.best_next[i, m]
				else:
					self.val_sum[i, m] += self.res_y[i] / self.probs[i, m]

				if self.alg['name'] == 'exp3elm':
					best_task = np.argmax(self.val_sum[i, :])
					max_val = self.val_sum[i, best_task]
					self.V[i, m] += 1 / self.probs[i, m]

					self.prev_V[i, m] = self.V[i, m]

					if max_val - self.val_sum[i, m] > np.sqrt(self.B * (self.V[i, best_task] - self.V[i, m])):
						self.active_tasks[i].remove(m)
				elif self.alg['name'] == 'exp3++':
					reward = self.res_y[i]
					loss = (1 - reward) / self.probs[i, m]

					self.prev_loss_sum[i, m], self.loss_sum[i, m] = self.loss_sum[i, m], self.loss_sum[i, m] + loss

					loss = 1 - self.res_y[i]
					self.prev_L_hat[i, m], self.L_hat[i, m] = self.L_hat[i, m], self.prev_L_hat[i, m] + loss
					self.prev_N_seen[i, m], self.N_seen[i, m] = self.N_seen[i, m], self.prev_N_seen[i, m] + 1

				self.S_c[k, i, m] = self.C[idx_C]
				self.S_y[k, i, m] = self.res_y[i] if self.res_y[i] > 0 else 0

				if self.TA_rule == 'max_prev+forget':
					if len(self.TA_rule_win_container_y[i][m]) < self.TA_rule_win:
							self.TA_rule_win_container_y[i][m].append(self.S_y[k, i, m])
							self.TA_rule_win_container_y[i][m][:-1] = [ self.TA_rule_discount_factor * elt for elt in self.TA_rule_win_container_y[i][m][:-1] ]

							self.TA_rule_win_container_c[i][m].append(self.S_c[k, i, m])
					else:
						self.TA_rule_win_container_y[i][m][:-1] = [ self.TA_rule_discount_factor * elt for elt in self.TA_rule_win_container_y[i][m][1:] ]
						self.TA_rule_win_container_y[i][m][-1] = self.S_y[k, i, m]

						self.TA_rule_win_container_c[i][m][:-1] = self.TA_rule_win_container_c[i][m][1:]
						self.TA_rule_win_container_c[i][m][-1] = self.S_c[k, i, m]

				# If waiting add value to induce exploration
				self.best_next[i][:m] += self.wait_expl_rate
				self.best_next[i][m+1:] += self.wait_expl_rate
	
				if verbose and (k % v_interval == 0):
					print(f'Round: {k:7}/{self.num_rounds}', end='\r')

				self._update_best_next(i, m)

				cur_bo.update_posterior()


			# For inspection of individual rounds
			# print(k, 'CTA assignment', self.t)
			# if opt_assign is not None:
			# 	print(k, 'opt assignment', opt_assign[k])
			# print(k, 'CTA comp', self.C)
			# if opt_comp is not None:
			# 	print(k, 'opt comp', opt_comp[k])

			# Reset compensations
			self.C_p = np.zeros(self.N)
			self.C = np.zeros(self.M)
			self.m_chosen = np.zeros(self.M, dtype=np.int32)

			
		print(80*'-')
		print('Finished!')
				
		return self.bo, self.S_c, self.S_y