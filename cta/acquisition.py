import numpy as np
import scipy as sp

from scipy.stats import norm



def random_AF(domain):
    '''Randomly chooses the next x to sample.'''
    x = rng.uniform(*domain)
    return x


def EI(x, prev_max, mu, sigma_sq):
    '''
    Expected improvement acquisition function.

    Args:
        x (float or np.ndarray): point at which the expected improvement is computed
        prev_max (float): largest previous evaluation 
        mu (function): mean function of the GP
        sigma_sq (function): variance function, i.e. kernel function applied to the same input k(x, x)
    '''
    delta = mu(np.array([x]).ravel().reshape(-1, 1)) - prev_max
    delta_pos = delta if delta > 0 else 0
    
    sigma_x = np.sqrt(sigma_sq(np.array([x]).reshape(-1, 1)))
    
    term_2 = sigma_x * norm.cdf(delta/sigma_x) if sigma_x > 0 else 0
    term_3 = np.abs(delta) * norm.pdf(delta/sigma_x) if sigma_x > 0 else 0

    res = np.ravel(delta_pos + term_2 - term_3)[0]
    return res


def PI(x, prev_max, mu, sigma_sq):
    '''
    Probability of improvement acquisition function.

    Args:
        x (float or np.ndarray): point at which the probability of improvement is computed
        prev_max (float): largest previous evaluation 
        mu (function): mean function of the GP
        sigma_sq (function): variance function, i.e. kernel function applied to the same input k(x, x)
    '''
    delta = mu(np.array([x]).ravel().reshape(-1, 1)) - prev_max
    # delta_pos = delta if delta > 0 else 0
    
    sigma_x = np.sqrt(sigma_sq(np.array([x]).reshape(-1, 1)))

    return norm.cdf(delta/sigma_x)[0] if sigma_x > 0 else 0


def UCB(x, prev_max, mu, sigma_sq, alpha=1):
    '''
    Upper confidence bound as an acquisition function.
    
    Args:
        x (float or np.ndarray): point at which the UCB is computed
        prev_max (float): largest previous evaluation 
        mu (function): mean function of the GP
        sigma_sq (function): variance function, i.e. kernel function applied to the same input k(x, x)
        alpha (float): parameter weighing the exploration-exploitation trade-off. Higher values induce exploration.
    '''
    tmp_mu = mu(np.array([x]).ravel().reshape(-1, 1))
    
    sigma_x = np.sqrt(sigma_sq(np.array([x]).reshape(-1, 1)))

    return (tmp_mu + (alpha * sigma_x))[0]


def create_biased_AF(comp_weigh_fun=lambda x: x**2, AF=EI):
    '''
    Includes bias towards lower compensation to the acquisition function.

    Args:
        comp_weigh_fun (function): defines how much weights should be attached to the issued compensation
    '''
    def f(x, prev_max, mu, sigma_sq):
        return AF(x, prev_max, mu, sigma_sq) / comp_weigh_fun(x)
    
    return f