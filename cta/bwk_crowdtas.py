import numpy as np
from scipy.optimize import linprog


from acquisition import *
from assignment import CrowdTAs

class BwKCrowdTAs(CrowdTAs):

    def __init__(self, num_workers, num_tasks, num_rounds, worker_funs, eval_funs, VFO_args, budgets, workers_per_task,
                 keep_in_memory=None, TA_rule='mean', TA_rule_discount_factor=0.9, TA_rule_win=50, domain=(5, 30), 
                 assure_iden_comp=False, wait_expl_rate=0.0001,
                # bwk specific
                 task_priorities=None, 
                 random_state=500):
        '''
        Implementation of Bandits with Knapsack. Only works with Bayesian optimization as the VFO method.

        Args:
            workers_per_task: determines a number of workers that should work at a certain task
                - can be considered as a desired load-balancing profile (and can change over rounds)
            task_priorities (np.ndarray): tasks ordered by priority. If `none` algorithm determines the priority.
        '''

        super().__init__(num_workers, num_tasks, num_rounds, worker_funs, eval_funs, VFO_args,
                 keep_in_memory=keep_in_memory, TA_rule=TA_rule, TA_rule_discount_factor=TA_rule_discount_factor, TA_rule_win=TA_rule_win,
                 domain=domain, assure_iden_comp=assure_iden_comp, wait_expl_rate=wait_expl_rate,
                 random_state=random_state)


        self.budgets = budgets
        self.workers_per_task = workers_per_task
        self.priorities = task_priorities
        self.priorities_at_init = self.priorities

        self.budgets_per_round = self.budgets / self.num_rounds

        self.r_avg = np.ones((self.M, self.N))
        self.r_history = [[[] for n in range(self.N)] for m in range(self.M)]

        self.C_avg = (np.ones((self.M, self.N)).T * self.budgets_per_round).T
        self.C_history = [[[] for n in range(self.N)] for m in range(self.M)]

        self.times_assigned = np.zeros((self.M, self.N))



    def run(self, opt_assign=None, opt_comp=None, verbose=False, v_interval=10, plot=None):

        for k in range(1, self.num_rounds):

            assigned_workers = []
            available_n = {n for n in range(self.N)}
            probs = np.ones((self.M, self.N))
            
            
            tmp = []
            # Determine in every round dynamically the probabilities
            for m in range(self.M):

                res = linprog(c=-self.r_avg[m], A_ub=self.C_avg[m:m+1, :], b_ub=self.budgets_per_round[m], A_eq=np.ones((1, self.N)), b_eq=1, x0=np.ones(self.N) / self.N)

                if not res.success:
                    raise ValueError('Budget exhausted!')
                probs[m] = res.x

                # Probability-weighted heuristic
                tmp.append((m, probs[m, :] @ self.best_next[:, m]))


            # Determine priorities according to probabilities
            if self.priorities_at_init is None:
                tmp.sort(key=lambda el: -el[1])
                self.priorities = list(zip(*tmp))[0]

            # Budget and constraints per task
            # Tasks should be selected according to the established priorities
            for m in self.priorities:

                # Worker selection while considering possible limit to the num of workers
                # per task
                for n in assigned_workers:
                    probs[m, n] = 0
                probs[m] = probs[m] / sum(probs[m])

                for _ in range(self.workers_per_task[m]):
                    chosen_worker = self.rng.choice(self.N, p=probs[m])

                    # Same worker cannot be selected multiple times
                    assigned_workers.append(chosen_worker)
                    probs[:, chosen_worker] = 0
                    for m_in in range(self.M):
                        probs[m_in] = probs[m_in] / sum(probs[m_in])

                    # Observations
                    cur_bo = self.bo[chosen_worker, m]
                    c, y, max_af_val = cur_bo.next(k)
                    self.S_c[k, chosen_worker, m] = c
                    self.S_y[k, chosen_worker, m] = y if y > 0 else 0

                    if self.TA_rule == 'max_prev+forget':
                        if len(self.TA_rule_win_container_y[chosen_worker][m]) < self.TA_rule_win:
                                self.TA_rule_win_container_y[chosen_worker][m].append(self.S_y[k, chosen_worker, m])
                                self.TA_rule_win_container_y[chosen_worker][m][:-1] = [ self.TA_rule_discount_factor * elt for elt in self.TA_rule_win_container_y[chosen_worker][m][:-1] ]

                                self.TA_rule_win_container_c[chosen_worker][m].append(self.S_c[k, chosen_worker, m])
                        else:
                            self.TA_rule_win_container_y[chosen_worker][m][:-1] = [ self.TA_rule_discount_factor * elt for elt in self.TA_rule_win_container_y[chosen_worker][m][1:] ]
                            self.TA_rule_win_container_y[chosen_worker][m][-1] = self.S_y[k, chosen_worker, m]

                            self.TA_rule_win_container_c[chosen_worker][m][:-1] = self.TA_rule_win_container_c[chosen_worker][m][1:]
                            self.TA_rule_win_container_c[chosen_worker][m][-1] = self.S_c[k, chosen_worker, m]


                    # If waiting add value to induce exploration
                    self.best_next[chosen_worker][:m] += self.wait_expl_rate
                    self.best_next[chosen_worker][m+1:] += self.wait_expl_rate
                    
                    self._update_best_next(chosen_worker, m)


                    cur_bo.update_posterior()


                    self.C_history[m][chosen_worker].append(c)
                    self.r_history[m][chosen_worker].append(y)

                    self.C_avg[m, chosen_worker] = np.average(self.C_history[m][chosen_worker])
                    self.r_avg[m, chosen_worker] = np.average(self.r_history[m][chosen_worker])

                    self.t[chosen_worker] = m



            if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{self.num_rounds}', end='\r')


            # For inspection of individual rounds
            # print(k, 'CTA assignment', self.t)
            # if opt_assign is not None:
            #     print(k, 'opt assignment', opt_assign[k])
            # if opt_comp is not None:
            #     print(k, 'opt comp', opt_comp[k])

            if plot is not None:
                bo_plot = BayOptPlot(self.bo[0, 0], self.num_rounds, self.S_c[0], self.S_y[0], plot_interval=1)

        if verbose:
            print(f'Finished BwKCrowdTAs with N = {self.N}, M = {self.M}, K = {self.num_rounds}')

                    
        return self.bo, self.S_c, self.S_y