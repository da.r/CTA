from visual.visualization import *
from environment_manager import *

from vfo import Zooming

import timeit


if __name__ == '__main__':
    # Settings
    N = 1
    M = 1
    plot_every_k_rounds = 1
    plot_refresh_time = 0.5

    rounds = 300
    rng = np.random.default_rng(500)
    domain = (5, 30)

    time_change_fun_no_noise = lambda k, x: x
    y_noise_fun = lambda prev_res: prev_res + rng.uniform(-0.1, 0.1)
    
    # Dynamic scenario
    # time_change_fun = lambda k, x: x - (k > 15)*3
    # Static scenario
    time_change_fun = lambda k, x: x

    W, W_no_noise = generate_workers(N, M, funs=(bounded_logistic_cost, ), param_bounds=(((7, 10), (0.5, 5)), (1, 20)),
                         verbose=True, time_change_fun=time_change_fun, without_noise_fun=time_change_fun_no_noise, y_noise_fun=y_noise_fun, random_state=500)

    zooming = Zooming(domain, eval_fun=W[0, 0], conf_importance=0.5, neighborhoods=20, init_active=2)


    wr_X, wr_y = plot_zooming(zooming, W_no_noise[0, 0], rounds=rounds, domain=domain, plot_interval=plot_every_k_rounds, refresh_time=plot_refresh_time)
    print(f'Run finished:')
    print(f'\tCompensation issued after {rounds} rounds: {sum(wr_X):.2f}')
    print(f'\tValue after {rounds} rounds: {sum(wr_y):.2f}')