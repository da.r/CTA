import numpy as np
import matplotlib.pyplot as plt

# from concurrent.futures import as_completed, ProcessPoolExecutor, wait

from abc import ABC, abstractmethod
from scipy.optimize import minimize_scalar, minimize

from acquisition import *
from vfo import BayOpt, Zooming
from visual.visualization import *
from environment_manager import *

import timeit

# NUM_CORES = 2


class Assignment(ABC):
    '''
    Abstract assignment class. Different assignment classes should implement it.
    '''
    def __init__(self, num_workers, num_tasks, num_rounds, actual_funs=None, keep_in_memory=None, lb_profiles=None):
        '''
        Initializes task assignment instance.

        Args:

        '''
        self.N = num_workers
        self.M = num_tasks
        self.num_rounds = num_rounds
        self.memory = num_rounds if keep_in_memory is None else keep_in_memory
        
        self.actual_funs = actual_funs
        self.lb_profiles = lb_profiles
        
        self.S_c = np.zeros((self.memory, self.N, self.M))
        self.S_y = np.zeros((self.memory, self.N, self.M))
    
    @abstractmethod
    def run(self):
        pass



class OptimalAssignment(Assignment):

    def __init__(self, num_workers, num_tasks, num_rounds, actual_funs, eval_funs, domain, lb_profiles=None, 
                 same_over_rounds=False, verbose=False, v_interval=10):
        '''
        Represents an optimal solution. Accepts `lb_profiles` to compute optimal assignment (using WORKER-SELECT greedy 
        approach) under the given sequence of profiles.

        Args:
            same_over_rounds (bool): speeds up the exhaustive search needed for optimal assignment. Can be # used in a
                static setting when value functions do not change over rounds.
        '''
        super().__init__(num_workers, num_tasks, num_rounds, actual_funs, lb_profiles=lb_profiles)

        self.eval_funs = eval_funs

        self.S_c = np.zeros((self.memory, self.N, self.M))
        self.S_y = np.zeros((self.memory, self.N, self.M))


        self.opt_C = np.empty((num_rounds, num_workers, num_tasks))
        self.opt_y = np.empty((num_rounds, num_workers, num_tasks))

        if same_over_rounds:
            tmp_C, tmp_y = compute_optimal_comp(actual_funs, domain, 0, return_values=True, quiet=True)
        for k in range(num_rounds):
            if same_over_rounds:
                self.opt_C[k], self.opt_y[k] = tmp_C, tmp_y
            else:    
                self.opt_C[k], self.opt_y[k] = compute_optimal_comp(actual_funs, domain, k, return_values=True, 
                                                                    quiet=True)
            if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{self.num_rounds}', end='\r')

        if self.lb_profiles is not None:

            self.lb_pairs = []

            for k in range(num_rounds):
                tmp = {(n, m): self.opt_y[k, n, m] for n in range(self.N) for m in range(self.M)}
                self.lb_pairs.append(sorted(tmp.items(), key=lambda el: -el[1]))
        
        print(80*'-')
        print('Finished instantiating OptimalAssignment!')

    def _get_current_profile(self, k):
        cur_profile = self.lb_profiles[(k - 1) % len(self.lb_profiles)]
        if 1 - np.sum(self.lb_profiles) < 0.01:
            round_profile = np.round(cur_profile * self.N)
            num_assignments = np.sum(round_profile)
            extra = int(self.N - num_assignments)
        else:
            round_profile = cur_profile
            extra = 0

        return round_profile, extra

    def run(self, verbose=True, v_interval=10, return_history=False):
        if return_history:
            assign_history = np.zeros((self.num_rounds, self.N))
            comp_history = np.zeros((self.num_rounds, self.N))

        for k in range(1, self.num_rounds):

            if self.lb_profiles is None:

                for n in range(self.N):
                    opt_t = np.argmax(self.opt_y[k, n])
                    best_c = self.opt_C[k, n, opt_t]

                    if return_history:
                        assign_history[k, n] = opt_t
                        comp_history[k, n] = best_c

                    self.S_c[k, n, opt_t] = best_c

                    # We use actual functions to keep it constant over runs when regret is computed.
                    evaluation = self.actual_funs[n, opt_t](k, best_c)

                    assert evaluation == self.opt_y[k, n, opt_t], 'Evaluation with actual function should be the\
                        same as the one produced by worker generation.'
                    self.S_y[k, n, opt_t] = evaluation if evaluation > 0 else 0

            else:
                round_profile, extra = self._get_current_profile(k)

                # Reset
                self.assigned = np.zeros(self.N, dtype=bool)
                
                for (n, m), _ in self.lb_pairs[k]:

                    if (self.assigned[n] or round_profile[m] == 0) and extra == 0:
                        continue

                    opt_t = m
                    best_c = self.opt_C[k, n, opt_t]

                    self.assigned[n] = True
                    if extra > 0:
                        extra -= 1
                    else:
                        round_profile[m] -= 1

                    if return_history:
                        assign_history[k, n] = opt_t
                        comp_history[k, n] = best_c

                    self.S_c[k, n, opt_t] = best_c

                    evaluation = self.eval_funs[n, opt_t](k, best_c)
                    self.S_y[k, n, opt_t] = evaluation if evaluation > 0 else 0

            if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{self.num_rounds}', end='\r')
        
        print(80*'-')
        print('Finished running OptimalAssignment!')

        if return_history:
            return self.S_c, self.S_y, assign_history, comp_history, self.opt_C, self.opt_y

        return self.S_c, self.S_y



class RandomAssignment(Assignment):
    
    def __init__(self, num_workers, num_tasks, num_rounds, actual_funs, domain=(5, 200), C=None, 
                 assure_iden_comp=False, lb_profiles=None, random_state=500):
        '''
        Baseline random assignment. Compensation can be preset. For example, one can precompute optimal compensations 
        for every worker-task pair and use those compensations combined with random assignment.

        Args:
            C (np.ndarray): contains preset compensation. If scalar, same compensation always assigned. If 2D-matrix,
                that compensation applied at all rounds. 3D-matrix can be given if compensations differ over rounds. 
            assure_iden_comp (bool): only assures the same dimensionality here, results do not depend on it, always the
                optimal compensation assigned
        '''
        super().__init__(num_workers, num_tasks, num_rounds, actual_funs, lb_profiles=lb_profiles)
        
        self.rng = np.random.default_rng(random_state)
        
        self.domain = domain
        self.assure_iden_comp = assure_iden_comp
        
        self.C = C


    def _get_current_profile(self, k):
        ''' 
        Given current round `k`, selects which profile in the crowdsourcing process should be assigned at round `k`.
        '''
        cur_profile = self.lb_profiles[(k - 1) % len(self.lb_profiles)]
        if 1 - np.sum(self.lb_profiles) < 0.01:
            round_profile = np.round(cur_profile * self.N)
            num_assignments = np.sum(round_profile)
            extra = int(self.N - num_assignments)
        else:
            round_profile = cur_profile
            extra = 0

        return round_profile, extra


            
    def run(self, verbose=False, v_interval=10):
        assignment = np.empty(self.N, dtype=np.int32)
        for k in range(1, self.num_rounds):
            assignment[:] = self.rng.choice(self.M, size=self.N)
            if self.C is None:
                if self.assure_iden_comp:
                    tmp_C = self.rng.uniform(*self.domain, size=self.M)
                    # To enforce the same dimensionality
                    compensation = [tmp_C[assignment[i]] for i in range(self.N)]
                else:
                    compensation = self.rng.uniform(*self.domain, size=self.N)
            elif self.C.ndim == 1:
                compensation = [self.C[assignment[i]] for i in range(self.N)]
            # Optimal for one round
            elif self.C.ndim == 2:
                compensation = [self.C[i, assignment[i]] for i in range(self.N)]
            # Optimal for every round
            elif self.C.ndim == 3:
                compensation = [self.C[k, i, assignment[i]] for i in range(self.N)]
            else:
                raise ValueError('C should have 1 (of length M), 2 (N, M) or 3 (K, N, M) dimensions')
                
            # Observe
            if self.lb_profiles is None:
                for i in range(self.N):
                    self.S_c[k, i, assignment[i]] = compensation[i]

                    evaluation = self.actual_funs[i, assignment[i]](k, compensation[i])
                    self.S_y[k, i, assignment[i]] = evaluation if evaluation > 0 else 0

            else:

                round_profile, extra = self._get_current_profile(k)

                # Reset
                available_n = {n for n in range(self.N)}

                for m, n_for_m in enumerate(round_profile):
                    ns_assigned = self.rng.choice(list(available_n), size=int(n_for_m), replace=False)

                    for n in ns_assigned:
                        self.S_c[k, n, m] = compensation[n]

                        evaluation = self.actual_funs[n, m](k, compensation[n])
                        self.S_y[k, n, m] = evaluation if evaluation > 0 else 0

                    # Set difference
                    available_n -= set(ns_assigned)

                for _ in range(extra):
                    m = self.rng.choice(self.M)
                    n = self.rng.choice(available_n)
                    self.S_c[k, n, m] = compensation[n]

                    evaluation = self.actual_funs[n, m](k, compensation[n])
                    self.S_y[k, n, m] = evaluation if evaluation > 0 else 0

                    available_n.remove(n)


        
            if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{self.num_rounds}', end='\r')
        
        print(80*'-')
        print('Finished!')
        return self.S_c, self.S_y



class RRAssignment(Assignment):
    '''
    Round Robin Task Assignment
    3 options:
        - random compensation in a domain range
        - preset compensation
            1. assuming workers known by setting function maxima 
            2. some preset compensation that represents what is the CS willing to pay for a task

    Args:
        assure_iden_comp (bool): only assures the same dimensionality here, results do not depend on it, always the
                optimal compensation assigned
    '''
    def __init__(self, num_workers, num_tasks, num_rounds, actual_funs, C=None,
                 domain=(5, 200), assure_iden_comp=False, lb_profiles=None, random_state=500):
        super().__init__(num_workers, num_tasks, num_rounds, actual_funs, lb_profiles=lb_profiles)
        
        self.rng = np.random.default_rng(random_state)
        
        self.domain = domain
        self.assure_iden_comp = assure_iden_comp
        
        self.C = C
    
    def run(self, verbose=False, v_interval=10):
        assignment = np.concatenate((np.tile(np.arange(self.M), self.N//self.M), np.arange(self.N%self.M)))
        compensation = np.zeros(self.N)
        for k in range(1, self.num_rounds):
            assignment[1:] = assignment[0:-1]
            assignment[0] = (k+1) % self.M
            
            if self.C is None:
                if self.assure_iden_comp:
                    tmp_C = self.rng.uniform(*self.domain, size=self.M)
                    # To enforce the same dimensionality
                    compensation = [compensation[assignment[i]] for i in range(self.N)]
                else:
                    compensation = self.rng.uniform(*self.domain, size=self.N)
            elif self.C.ndim == 1:
                compensation = [self.C[assignment[i]] for i in range(self.N)]
            # Optimal for one round
            elif self.C.ndim == 2:
                compensation = [self.C[i, assignment[i]] for i in range(self.N)]
            # Optimal for every rounds
            elif self.C.ndim == 3:
                compensation = [self.C[k, i, assignment[i]] for i in range(self.N)]
            else:
                raise ValueError('C should have 1 (of length M), 2 (N, M) or 3 (K, N, M) dimensions')
            
            # Observation        
            if self.lb_profiles is None:

                for i in range(self.N):
                    self.S_c[k, i, assignment[i]] = compensation[i]

                    evaluation = self.actual_funs[i, assignment[i]](k, compensation[i])
                    self.S_y[k, i, assignment[i]] = evaluation if evaluation > 0 else 0

            else:
                raise ValueError('Round robin not applicable to an existing load balance.')
                    
            if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{self.num_rounds}', end='\r')

        
        print(80*'-')
        print('Finished!')
        return self.S_c, self.S_y



class CrowdTAs(Assignment):
    def __init__(self, num_workers, num_tasks, num_rounds, actual_funs, eval_funs, VFO_args, lb_profiles=None, 
                 keep_in_memory=None, domain=(5, 30), assure_iden_comp=True, TA_rule='mean', TA_rule_win=50, 
                 TA_rule_discount_factor=0.9, wait_expl_rate=0.001, VFO_method='bayopt', TA_method='adex', 
                 must_assign_all=False, random_state=500):
        '''
        Assignment class that implement the algorithms from our solution approach.

        Args:
            num_workers (int): number of workers
            num_tasks (int): number of tasks
            num_rounds (int): number of rounds
            actual_funs (np.array(function)): a matrix of actual value functions
            eval_funs (np.array(function)): a matrix containing evaluation function, i.e. actual value functions with 
                the effect of evaluation noise
            VFO_args (dict): dictionary containing arguments of the used value function optimization method
            lb_profiles (np.ndarray): represents a crowdsourcing process as a sequence of load balancing profile. Rows 
                in the matrix represent load balancing profiles, with the first row being the first profile in the 
                sequence.
            keep_in_memory (np.ndarray): not used anymore, previously allowed specification of memory
            domain (tuple): domain range represented as a tuple (lower bound, upper bound)
            assure_iden_comp (bool): if True fairness constraint is taken into account
            TA_rule (string): specifies the used task assignment heuristics:
                - `'mean'`: according to the mean of the GP (only BayOpt)
                - `'af'`: according to the maximum value of the acquisition function (only BayOpt)
                - `'max_prev'`: according to the maximum previous observation
                - `'max_prev+af'`: sums `max_prev` and `max_af` (only BayOpt)
                - `'max_prev+forget'`: according to the maximum previous observation in memory
                - `'max_avg+forget'`:according to the maximum average of observations in memory
            TA_rule_win (int): size of the sliding window used in `'+forget'` TA_rule heuristics
            TA_rule_discount_factor (float): specifies how much is the considered value of the old observations decreased 
                in every round. Can be used only in `'+forget'` TA_rule heuristics.
            TA_method (string): task assignment method, either `'adex'` (from ADaptable EXplore) or `'ucb'`
            must_assign_all (bool): whether at least one worker has to be assigned to all of the tasks. 
                Assumes that N > M.
            random_state (int): seed for random number generator
        '''
        super().__init__(num_workers, num_tasks, num_rounds, actual_funs, keep_in_memory)
        
        self.S_c = np.zeros((self.memory, self.N, self.M))
        self.S_y = np.zeros((self.memory, self.N, self.M))
        
        self.rng = np.random.default_rng(random_state)

        self.VFO_method = VFO_method
        if VFO_method == 'bayopt':
            self.bo = np.array([[BayOpt([self.rng.uniform(*domain)], domain, actual_funs[j, i], eval_funs[j, i], **VFO_args)
                                 for i in range(self.M)] 
                                  for j in range(self.N)])
        elif VFO_method == 'zooming':
            self.zooming = np.array([[Zooming(domain, eval_funs[j, i], **VFO_args)
                for i in range(self.M)]
                    for j in range(self.N)])
        else:
           raise ValueError(f'Given VFO method does not exist. Given: `{VFO_method}`')
        

        self.domain = domain
        self.assure_iden_comp = assure_iden_comp

        self.TA_rule = TA_rule
        self.TA_method = TA_method
        self.TA_rule_discount_factor = TA_rule_discount_factor
        self.TA_rule_win = TA_rule_win
        self.TA_rule_win_container_y = [[[0,] for i in range(self.M)] for j in range(self.N)]
        self.TA_rule_win_container_c = [[[0,] for i in range(self.M)] for j in range(self.N)]
        
        # Preset to induce exploration in beginning (can be a parameter)
        self.best_next = np.array([[float('inf')
                           for i in range(self.M)] 
                            for j in range(self.N)], dtype=np.float64)
        
        self.best_next_c = np.array([[(domain[1]-domain[0])/4
                           for i in range(self.M)] 
                            for j in range(self.N)], dtype=np.float64)
        
        self.t = np.array([np.argmax(self.best_next[i]) for i in range(self.N)])

        self.wait_expl_rate = wait_expl_rate

        self.times_assigned = np.ones((self.N, self.M))

        self.adaptable_explore = len(self.t) * [0.5]
        self.prev_rand_choice = self.t.copy()
        self.prev_max = self.t.copy()

        self.max_af_val = len(self.t) * [0.5]
        self.res_x = len(self.t) * [0]
        self.res_y = len(self.t) * [0]

        self.must_assign_all = must_assign_all
        self.lb_profiles = lb_profiles
        self.m_chosen = np.zeros(self.M, dtype=np.int32)

        self.pairs = {(n, m): self.best_next[n, m] for n in range(self.N) for m in range(self.M)}
        # Per round
        self.workers_assigned = np.zeros(self.N, dtype=bool)
        self.tasks_available = np.ones(self.M, dtype=bool)

    def _get_current_profile(self, k):
        ''' 
        Given current round `k`, selects which profile in the crowdsourcing process should be assigned at round `k`.
        '''
        cur_profile = self.lb_profiles[(k - 1) % len(self.lb_profiles)]
        if 1 - np.sum(self.lb_profiles) < 0.01:
            round_profile = np.round(cur_profile * self.N)
            num_assignments = np.sum(round_profile)
            extra = int(self.N - num_assignments)
        else:
            round_profile = cur_profile
            extra = 0

        return round_profile, extra


    def _assure_iden_comp(self, C_p, k, C, round_profile=None):
        '''
        In-place implementation of ASSURE-IDEN-COMP. Evaluates and assigns tasks.

        Args: 
            C_p (np.ndarray): (N-dimensional) proposed compensations for every worker
            k (int): current round
            C (np.ndarray): C (M-dimenstional) vector to which the issued compensations per task will be issued
            round_profile (np.ndarray): sequence of lb profiles as a matrix with rows representing individual 
            load balancing profiles from top to bottom
        '''

        max_val = np.zeros(self.M)

        evaluated = [False] * self.N
        if round_profile is not None:
            # Reset available tasks
            self.tasks_available = np.ones(self.M, dtype=bool)
        
        # Each is chosen at least once, so enough to remember previous one
        prev_chosen = [[] for i in range(self.M)]
        for i in range(self.N):
            # Previous task assignment proposition
            m_in = self.t[i]

            if max_val[m_in] <= self.best_next[i, m_in]:

                max_val[m_in] = self.best_next[i, m_in]
                C[m_in] = C_p[i]
                
                prev_chosen[m_in].append(i)
            else:
                # To keep it sorted with the best at last, swap last if not better
                prev_chosen[m_in].append(i)
                prev_chosen[m_in][-2], prev_chosen[m_in][-1] = prev_chosen[m_in][-1], prev_chosen[m_in][-2]

        # Remove last added (i.e. best next choice)
        for m in range(self.M):

            if prev_chosen[m]:

                if round_profile is not None:
                    round_profile[m] -= 1
                    if round_profile[m] == 0:
                        self.tasks_available[m] = False

                i_best = prev_chosen[m].pop()

                # Evaluate best choices
                if self.VFO_method == 'bayopt':
                    self.res_x[i_best], self.res_y[i_best], self.max_af_val[i_best] =  self.bo[i_best, m].next(k)
                elif self.VFO_method == 'zooming':
                    self.res_x[i_best], self.res_y[i_best] =  self.zooming[i_best, m].next(k)
                C[m] = self.res_x[i_best]
                evaluated[i_best] = True


        # Select the best of remaining options
        for m in range(self.M):
            for n in prev_chosen[m]:

                if evaluated[n] or (not self.tasks_available[m]):
                    continue

                val_options = np.zeros(self.M)
                for m_in in range(self.M):
                    # If there are tasks that were not selected, 
                    # i.e. unassigned compensations
                    val_options[m_in] = self.best_next[n, m_in]

                m_chosen = np.argmax(val_options)
                self.t[n] = m_chosen

                # EVALUATE the remaining choices
                # -----------------------------------------------------------
                # Now becomes greedy (set immediately don't look further recursively for possible 
                # improvement if this task assigned to other workers)
                # Let VFO algorithm choose C
                if C[m_chosen] == 0:
                    # C[m_chosen] = c_options[m_chosen]
                    if self.VFO_method == 'bayopt':
                        self.res_x[n], self.res_y[n], self.max_af_val[n] = self.bo[n, m_chosen].next(k)
                    elif self.VFO_method == 'zooming':
                        self.res_x[n], self.res_y[n] = self.zooming[n, m_chosen].next(k)
                    
                    C[m_chosen] = self.res_x[n]
                
                else:
                    if self.VFO_method == 'bayopt':
                        self.res_x[n], self.res_y[n], self.max_af_val[n] = self.bo[n, m_chosen].next(k, given_c=C[m_chosen])
                    elif self.VFO_method == 'zooming':
                        self.res_x[n], self.res_y[n] = self.zooming[n, m_chosen].next(k, given_c=C[m_chosen])

                if round_profile is not None:
                    round_profile[m] -= 1
                    if round_profile[m] == 0:
                        self.tasks_available[m] = False
                    
                    if np.any(round_profile < 0):
                        raise Exception(f'Load balancing profile constraint not fulfilled. Round profile: {round_profile}')


        
    def assure_iden_comp_fun(self, C_p, k, out=None, round_profile=None):
        if out is not None:
            self._assure_iden_comp(C_p, k, out, round_profile=round_profile)
        else:
            C = np.zeros(self.M)
            self._assure_iden_comp(C_p, k, C, round_profile=round_profile)
            return C


    def _update_best_next(self, i, m):
        '''
        Updates the `best_next` which is used for heuristics mu_v and mu_c in thesis.
        '''
        if self.TA_rule == 'mean':
            res_mean = minimize_scalar(lambda x: -self.bo[i, m].mu(np.array([x]).reshape(1,-1)), method='bounded', 
                                        bounds=self.domain)

            self.best_next[i, m] = -res_mean.fun
            self.best_next_c[i, m] = res_mean.x

        elif self.TA_rule == 'af':
            self.best_next[i, m] = self.max_af_val[i] if self.max_af_val[i] > 0 and self.max_af_val[i] != np.inf and self.max_af_val[i] != np.nan else 0
            self.best_next_c[i, m] = self.res_x[i]

        elif self.TA_rule == 'max_prev+af':
            self.best_next[i, m] = np.max(self.S_y[:, i, m])

            self.best_next[i, m] += self.max_af_val[i] if self.max_af_val[i] > 0 and self.max_af_val[i] != np.inf and self.max_af_val[i] != np.nan else 0
            self.best_next_c[i, m] = self.res_x[i]

        elif self.TA_rule == 'max_prev':
            k_max_seen = np.argmax(self.S_y[:, i, m])
            self.best_next[i, m] = self.S_y[k_max_seen, i, m]
            self.best_next_c[i, m] = self.S_c[k_max_seen, i, m]

        elif self.TA_rule == 'max_prev+forget':
            idx = np.argmax(self.TA_rule_win_container_y[i][m])
            self.best_next[i, m] = self.TA_rule_win_container_y[i][m][idx]
            self.best_next_c[i, m] = self.TA_rule_win_container_c[i][m][idx]

        elif self.TA_rule == 'max_avg+forget':
            self.best_next[i, m] = np.average(self.TA_rule_win_container_y[i][m])
            idx = np.argmax(self.TA_rule_win_container_y[i][m])
            self.best_next_c[i, m] = self.TA_rule_win_container_c[i][m][idx]

        elif self.TA_rule =='max_prev+forget+boAssigned':
            cur_bo = self.bo[i, m]
            idx = np.argmax(cur_bo.assigned_y)

            self.best_next[i, m] = cur_bo.assigned_y[idx]
            self.best_next_c[i, m] = cur_bo.assigned_X[idx]

        else:
            raise ValueError(f"TA_rule argument accepts only 'mean', 'af' and 'max_prev'. " +
                                 f"Given value: '{self.TA_rule}''")
    

    def task_assignment(self, i, k):
        '''
        Corresponds to TASK-ASSIGN in thesis. Sets preliminary next self.t[i]

        Args:
            i (int): worker i
            k (int): current round
        '''

        # Consider available tasks
        cur_best_next = np.where(self.tasks_available, self.best_next[i], -1)

        if self.TA_method == 'adex':
            if self.rng.uniform() < self.adaptable_explore[i]:
                probs = 1 / self.times_assigned[i][self.tasks_available]
                probs /= np.sum(probs)

                self.t[i] = self.rng.choice(np.arange(self.M)[self.tasks_available], p=probs)
                self.prev_rand_choice[i] = self.t[i]
            else:
                self.t[i] = np.argmax(cur_best_next)
                if self.prev_rand_choice[i] != -1:
                    if self.t[i] == self.prev_max[i]:
                        self.adaptable_explore[i] /= 1.02
                        # Could be a param that defnes the minimum amount of exploration.
                        self.adaptable_explore[i] = 0.05 if self.adaptable_explore[i] < 0.05 else self.adaptable_explore[i]

                    else:
                        self.adaptable_explore[i] *= 1.02
                        # Could be a param that defnes the minimum amount of exploration.
                        self.adaptable_explore[i] = 0.5 if self.adaptable_explore[i] > 0.5 else self.adaptable_explore[i]

                self.prev_max[i] = self.t[i]
                self.prev_rand_choice[i] = -1

        elif self.TA_method == 'ucb':
            var_term = np.sqrt( (2 * np.log(k)) /  self.times_assigned[i])
            var_term = np.where(self.tasks_available, var_term, 0)

            self.t[i] = np.argmax(cur_best_next + (1/self.domain[1]) * var_term)
            self.prev_rand_choice[i] = self.t[i]


        else:
            raise ValueError(f'TA_method can be "adex" or "UCB", but given: {self.TA_method}')

        # Update Last assigned
        m = self.t[i]
        self.C_p[i] = self.best_next_c[i, m]
        
    def run(self, plot=None, num_rounds=None, AF=None, assure_iden_comp=True, verbose=False, v_interval=10, opt_assign=None, opt_comp=None):
        '''Corresponds to the Algorithm 1, Chapter 4 in thesis, with the difference that all of the rounds are considered in this implementation.'''

        if num_rounds is not None:
            self.num_rounds = num_rounds
        
        # Compensation proposition
        self.C_p = np.zeros(self.N)
        self.C = np.zeros(self.M)

        if plot is not None and self.VFO_method == 'bayopt':
            bo_plot = BayOptPlot(self.bo[0, 0], self.num_rounds, self.S_c[0], self.S_y[0], plot_interval=1)
            
        
        for k in range(1, self.num_rounds):

            if self.lb_profiles is not None:
                round_profile, extra = self._get_current_profile(k)

                round_profile_aic = np.copy(round_profile)
            else:
                round_profile_aic = None


            # Could be parallelized in theory, but multiprocessing in python does not support the use of lambda 
            # functions inside other functions (like in the `generate_workers()` function).
            # 
            #     futures = {executor.submit(self.task_assignment, i) : i for i in range(self.N)}
            #     # wait(futures)
            #     for future in as_completed(futures, timeout=1):
            #         try:
            #             future.result()
            #         except Exception:
            #             raise future.exception()
            #         worker = futures[future]
            #         task = self.t[worker]
            #         self.m_chosen[task] += 1 




            # Load balancing
            # Corresponds to WORKER-SELECT algorithm in thesis
            if self.lb_profiles is not None:
                # Reset list of assigned workers
                self.workers_assigned = np.zeros(self.N, dtype=bool)
                self.tasks_available = np.where(round_profile > 0, True, False)

                for (n, m), _ in sorted(self.pairs.items(), key=lambda el: -el[1]):

                    if (self.workers_assigned[n] or round_profile[m] == 0) and extra == 0:
                        continue

                    # Using TA algo gives an opportunity for exploration sometimes
                    self.task_assignment(n, k)

                    self.m_chosen[self.t[n]] += 1
                    if round_profile[self.t[n]] == 1:
                        self.tasks_available[self.t[n]] = False

                    self.workers_assigned[n] = True

                    if extra > 0:
                        extra -= 1
                    else:
                        round_profile[self.t[n]] -= 1

                if np.any(round_profile < 0):
                    raise Exception(f'Load balancing profile constraint not fulfilled. Round profile: {round_profile}')

            else:
                # Tries to find the optimal load balancing ratio
                for i in range(self.N):
                    # Sets preliminary task assignment self.t[i]
                    self.task_assignment(i, k)
                    self.m_chosen[self.t[i]] += 1


            if self.must_assign_all:
                for m in range(self.M):

                    if self.m_chosen[m] == 0:

                        # Sorting, efficiency hit
                        workers_sorted = np.argsort(self.best_next[:, m])[::-1]

                        for i in workers_sorted:
                            # Swap only if giving task has enough workers
                            if self.m_chosen[self.t[i]] >= 2:
                                self.m_chosen[self.t[i]] -= 1
                                self.t[i] = m
                                self.m_chosen[m] += 1
                                break
                        else:
                            raise ValueError('There is not enough workers for the given number of tasks - "must_assign_all" cannot be True')

            else:
                pass



            if self.assure_iden_comp:
                self.assure_iden_comp_fun(self.C_p, k, out=self.C, round_profile=round_profile_aic)
                # To everyone assign the best at that compensation
                
            else:
                self.C = self.C_p


            # Observe and store
            vals = []
            for i in range(self.N):

                m = self.t[i]
                self.times_assigned[i, m] += 1

                if self.VFO_method == 'bayopt':
                    cur_bo = self.bo[i, m]
                elif self.VFO_method == 'zooming':
                    cur_zoom = self.zooming[i, m]

                if self.assure_iden_comp:
                    idx_C = m
                    # Already evaluated in assure_iden_comp -- evaluation = calling .next()
                else:
                    idx_C = i
                    # Let VFO method decide if equitable C not assured
                    if self.VFO_method == 'bayopt':
                        self.res_x[i], self.res_y[i], self.max_af_val[i] = cur_bo.next(k)
                    elif self.VFO_method == 'zooming':
                        self.res_x[i], self.res_y[i] = cur_zoom.next(k)

                    self.C[i] = self.res_x[i]
                    vals.append(self.res_y[i])

                if plot is not None and self.VFO_method == 'bayopt':
                    if i == plot[0] and m == plot[1]:
                        bo_plot.update_plot(cur_bo, k, self.res_x[i], self.res_y[i], self.max_af_val[i], self.S_c[k], self.S_y[k])


                
                self.S_c[k, i, m] = self.C[idx_C]
                self.S_y[k, i, m] = self.res_y[i] if self.res_y[i] > 0 else 0

                # Only if bayopt is used
                if self.TA_rule == 'max_prev+forget' or self.TA_rule == 'max_avg+forget':
                    if len(self.TA_rule_win_container_y[i][m]) < self.TA_rule_win:
                            self.TA_rule_win_container_y[i][m].append(self.S_y[k, i, m])
                            self.TA_rule_win_container_y[i][m][:-1] = [ self.TA_rule_discount_factor * elt for elt in self.TA_rule_win_container_y[i][m][:-1] ]

                            self.TA_rule_win_container_c[i][m].append(self.S_c[k, i, m])
                    else:
                        self.TA_rule_win_container_y[i][m][:-1] = [ self.TA_rule_discount_factor * elt for elt in self.TA_rule_win_container_y[i][m][1:] ]
                        self.TA_rule_win_container_y[i][m][-1] = self.S_y[k, i, m]

                        self.TA_rule_win_container_c[i][m][:-1] = self.TA_rule_win_container_c[i][m][1:]
                        self.TA_rule_win_container_c[i][m][-1] = self.S_c[k, i, m]


                # If waiting add value to induce exploration
                self.best_next[i][:m] += self.wait_expl_rate
                self.best_next[i][m+1:] += self.wait_expl_rate
    
                if verbose and (k % v_interval == 0):
                    print(f'Round: {k:7}/{self.num_rounds}', end='\r')

                self._update_best_next(i, m)
                if self.VFO_method == 'bayopt':
                    cur_bo.update_posterior()

            # For inspection
            # print(k, 'CTA assignment', self.t)
            # if opt_assign is not None:
            #     print(k, 'opt assignment', opt_assign[k])
            # print(k, 'CTA comp', self.C)
            # if opt_comp is not None:
            #     print(k, 'opt comp', opt_comp[k])
            # print(k, 'CTA val', vals)

            # Reset compensations
            self.C_p = np.zeros(self.N)
            self.C = np.zeros(self.M)
            self.m_chosen = np.zeros(self.M, dtype=np.int32)

        print(80*'-')
        print('Finished!')
        
        if self.VFO_method == 'bayopt':
            return self.bo, self.S_c, self.S_y
        elif self.VFO_method == 'zooming':
            return self.zooming, self.S_c, self.S_y