import inspect
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt




class TaskAssignmentSaverPlotter:

	def __init__(self, path, run_id, N, M, rounds, domain, TA_rule, TA_rule_win, TA_rule_discount_factor,
				 time_change_fun, time_change_prob, y_noise_fun, param_bounds, bayopt_args, zoom_args, random_state):
		'''
		Instead of storing the source code of the used setting, one could
		alternatively store the variables of this class.
		'''
		self.path = path
		self.run_id = run_id

		self.N = N
		self.M = M
		self.rounds = rounds
		self.domain = domain

		self.TA_rule = TA_rule
		self.TA_rule_win = TA_rule_win
		self.TA_rule_discount_factor = TA_rule_discount_factor

		self.time_change_fun = inspect.getsourcelines(time_change_fun)
		self.time_change_prob = time_change_prob
		self.y_noise_fun = inspect.getsourcelines(y_noise_fun)
		self.param_bounds = param_bounds

		self.bayopt_args = bayopt_args
		self.zoom_args = zoom_args

		self.random_state = random_state


	def plot_all_worker_funs(self, W, k=0, show=True):
		'''
		Plots all of the worker-task function versions associated with round `k`.

		Args:
			W (np.ndarray): array which contains all of the worker functions
			k (int): round at which the view of the worker functions is shown
			show (bool): determines whether the plot is shown after generation
		'''
		X = np.linspace(*self.domain, num=5_000)
		fig, ax = plt.subplots(1, 1)
		for i in range(W.shape[0]):
			for j in range(W.shape[1]):
				ax.plot(X, W[i, j](k, X), label=f'{i, j}')
		ax.set_title(f'Actual worker-task value function at round {k}')
		ax.set_xlabel('compensation')
		ax.set_ylabel('value')

		if show:
			plt.show()
		fig.savefig((self.path / (self.run_id + f'_k={k}' + '_workers.png')).resolve(), bbox_inches='tight')


	def plot_time_spent(self, ax, compensations, labels, line_infos, save_fig=False, fontsize='xx-small', markevery=None):
		'''
		Plots spent compensation against time.

		Args:
			save_fig (bool): if true ax is part of the newly created figure that is saved
		'''
		# Duplication needed because matplotlib doesn't allow axis sharing among figures
		if save_fig:
			fig, saved_ax = plt.subplots(1,1)
			for i in range(len(labels)):
				saved_ax.plot(np.arange(compensations[i].size), compensations[i], line_infos[i][1], markevery=markevery, color=line_infos[i][0], label=labels[i])
			saved_ax.legend(fontsize=fontsize)
			saved_ax.set_xlabel('time')
			saved_ax.set_ylabel('compensation')

		for i in range(len(labels)):
			ax.plot(np.arange(compensations[i].size), compensations[i], line_infos[i][1], markevery=markevery, color=line_infos[i][0], label=labels[i])
		ax.legend(fontsize=fontsize)
		ax.set_xlabel('time')
		ax.set_ylabel('compensation')

		if save_fig:
			fig.savefig((self.path / self.plot_type / (self.run_id + '_time_spent.png')).resolve(),
							 bbox_inches='tight')


	def plot_time_value(self, ax, values, labels, line_infos, save_fig=False, fontsize='xx-small', markevery=None):
		'''
		Plots value against time in rounds.

		Args:
			save_fig (bool): if true ax is part of the newly created figure that is saved
		'''
		# Duplication needed because matplotlib doesn't allow axis sharing among figures
		if save_fig:
			fig, saved_ax = plt.subplots(1,1)
			for i in range(len(labels)):
				saved_ax.plot(np.arange(values[i].size), values[i], line_infos[i][1], markevery=markevery, color=line_infos[i][0], label=labels[i])
			saved_ax.legend(fontsize=fontsize)
			saved_ax.set_xlabel('time')
			saved_ax.set_ylabel('value')

		for i in range(len(labels)):
			ax.plot(np.arange(values[i].size), values[i], line_infos[i][1], markevery=markevery, color=line_infos[i][0], label=labels[i])
		ax.legend(fontsize=fontsize)
		ax.set_xlabel('time')
		ax.set_ylabel('value')

		if save_fig:
			fig.savefig((self.path / self.plot_type / (self.run_id + '_time_value.png')).resolve(), 
							 bbox_inches='tight')


	def plot_spent_value(self, ax, compensations, values, labels, line_infos, save_fig=False, fontsize='xx-small', markevery=None):
		'''
		Plots value against issued compensation.
		Args:
			save_fig (bool): if true ax is part of the newly created figure that is saved
		'''
		# Duplication needed because matplotlib doesn't allow axis sharing among figures
		if save_fig:
			fig, saved_ax = plt.subplots(1,1)
			for i in range(len(labels)):
				saved_ax.plot(compensations[i], values[i], line_infos[i][1], markevery=markevery, color=line_infos[i][0], label=labels[i])
			saved_ax.legend(fontsize=fontsize)
			saved_ax.set_xlabel('compensation')
			saved_ax.set_ylabel('value')
			saved_ax.xaxis.set_tick_params(labelsize='xx-small', rotation=-45)

		for i in range(len(labels)):
			ax.plot(compensations[i], values[i], line_infos[i][1], markevery=markevery, color=line_infos[i][0], label=labels[i])
		ax.legend(fontsize=fontsize)
		ax.set_xlabel('compensation')
		ax.set_ylabel('value')
		ax.xaxis.set_tick_params(labelsize='xx-small', rotation=-45)

		if save_fig:
			fig.savefig((self.path / self.plot_type / (self.run_id + '_spent_value.png')).resolve(), 
							 bbox_inches='tight')

		

	def plot_eval(self, fig, axs, eval_objs, plot_type='avgValUntilK', optimal_y=None, save_figs=3*[False], show=False, fontsize='xx-small', markevery=None):
		'''
		Args:
			axs: should have 3 axes
			plot_type (string): three possible entries depending on the content of time-spent and time-value plots
				`'avgValUntilK'` - plots average values until round k
				`'instantValueRatio'` - plots value for the algorithm assignment divided by the value of the optimal
					asssignment for every round k. Makes reaction of the algorithm to dynamic changes visible; requires `optimal_y`
				`'cumulativeRegret' - plots cumulative regret; requires `optimal_y`
			optimal_y (np.ndarray, optional): values of the optimal run, required if `plot_type` is 'instantRegret' or 'cumulativeRegret'
		'''
		compensations = [np.cumsum(np.sum(eval_objs[label][2][0], axis=(1, 2))) for label in eval_objs]
		if plot_type == 'avgValUntilK':
			values = [np.cumsum(np.sum(eval_objs[label][2][1], axis=(1, 2)))/range(1, eval_objs[label][2][1].shape[0]+1) for label in eval_objs]
		elif plot_type == 'instantValueRatio':
			values = [(np.sum(eval_objs[label][2][1], axis=(1, 2))) / np.sum(optimal_y, axis=(1, 2)) for label in eval_objs]
		elif plot_type == 'cumulativeRegret':
			values = [np.cumsum(np.sum(optimal_y, axis=(1, 2)) - np.sum(eval_objs[label][2][1], axis=(1, 2))) for label in eval_objs]
		else:
			raise ValueError('Unknown `plot_type`.')
		
		self.plot_type = plot_type
		path = (self.path / plot_type).resolve()
		path.mkdir(parents=True, exist_ok=True)

		labels = [*eval_objs.keys()]
		line_infos = [eval_objs[label][0] for label in eval_objs]
		self.plot_time_spent(axs[0], compensations, labels, line_infos, save_figs[0], fontsize, markevery=markevery)
		self.plot_time_value(axs[1], values, labels, line_infos, save_figs[1], fontsize, markevery=markevery)
		self.plot_spent_value(axs[2], compensations, values, labels, line_infos, save_figs[2], fontsize, markevery=markevery)

		fig.savefig((path / f'evaluation.png'), bbox_inches='tight')

		# When the whole source file is saved, saving these settings is not needed anymore.
		# with open((self.path / f'{self.run_id}_settings.txt').resolve(), 'a') as f:
		# 	f.write(str(vars(self)))

		if show:
			plt.show()