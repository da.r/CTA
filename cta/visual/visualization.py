import numpy as np
from scipy.optimize import minimize, minimize_scalar

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from acquisition import EI
from vfo import *
from workers import *


def draw_posterior(ax, X, mu, sigma_sq, seen_X, seen_y):
    X = X.reshape(-1, 1)
    mean = mu(X).ravel()
    (line_mean, ) = ax.plot(X, mean, lw=3, label='mean function')
    (line_seen_pts, ) = ax.plot(seen_X[:-1], seen_y[:-1], marker='o', markersize=7, color='green', linestyle='None', label='seen points')
    (line_last_pt, ) = ax.plot(seen_X[-1], seen_y[-1], marker='o', markersize=13, color='orange', linestyle='None', label='last point')

    #  Draw max seen
    max_idx = np.argmax(seen_y)
    (line_max_pt, ) = ax.plot(seen_X[max_idx], seen_y[max_idx], marker='o', markersize=13, color='red', linestyle='None', label='max point')

    
    std_dev = np.diag(sigma_sq(X.reshape(-1,1)))
    std_dev_poly = ax.fill_between(X.ravel(), y1=mean-1.96*std_dev, y2=mean+1.96*std_dev, alpha=0.3, cmap='Blues')
    ax.legend()

    return line_mean, line_seen_pts, line_last_pt, line_max_pt, std_dev_poly


def redraw_posterior(ax, X_plt, mu, sigma_sq, seen_X, seen_y, line_mean, line_seen_pts, line_last_pt, line_max_pt, std_dev_poly, line_fun, fun_y):
    '''
    In-place. Uses settings in BayOpt instance.
    '''
    mean = mu(X_plt.reshape(-1, 1)).ravel()
    # ax.set_xlim(*domain)
    # ax.set_ylim(-0.05, 0.2)

    line_fun.set_data(X_plt, fun_y)
    line_mean.set_data(X_plt, mean)

    #  Draw max seen
    max_idx = np.argmax(seen_y)
    line_max_pt.set_data(seen_X[max_idx], seen_y[max_idx])

    line_seen_pts.set_data(seen_X[:-1], seen_y[:-1])
    line_last_pt.set_data(seen_X[-1], seen_y[-1])


    std_dev = np.diag(sigma_sq(X_plt.reshape(-1, 1)))
    std_dev_poly.remove()
    std_dev_poly = ax.fill_between(X_plt.ravel(), y1=mean-1.96*std_dev, y2=mean+1.96*std_dev, alpha=0.3, cmap='Blues')
    std_dev_poly.set_color('blue')

    return std_dev_poly

def _calc_af(X_plt, AF, bo):
    af_fun = lambda x: -AF(x, np.max(bo.memory_y), bo.mu, bo.sigma_sq)
    af_res = []
    for x in X_plt:
        af_res.append(-af_fun(x))
    af_res = np.array(af_res)
    return af_res

def draw_af(ax, X_plt, AF, bo):
    # Plot acquisition function
    af_res = _calc_af(X_plt, AF, bo)
    (af_line, ) = ax.plot(X_plt, af_res, 'y--', label='Acquisition Function')
    return af_line

def redraw_af(af_line, X_plt, AF, bo):
    '''In-place'''
    af_res = _calc_af(X_plt, AF, bo)
    af_line.set_data(X_plt, af_res)

def draw_img(ax_img_c, ax_img_y, S_c_init, S_y_init, vmin, vmax):
    img_c = ax_img_c.imshow(S_c_init, cmap='RdYlGn_r', vmin=vmin, vmax=vmax)
    img_y = ax_img_y.imshow(S_y_init, cmap='RdYlGn')

    return img_c, img_y

def redraw_img(img_c, img_y, S_c_cur, S_y_cur):
    '''In-place'''
    img_c.set_data(S_c_cur)
    img_y.set_data(S_y_cur)
    

class BayOptPlot:
    def __init__(self, bo, num_rounds, S_c_init, S_y_init, plot_interval=10, refresh_time=0.1):
        idx_best_seen = None

        self.bo = bo

        opt = minimize_scalar(lambda x: -bo.actual_fun(0, x), method='bounded', bounds=bo.domain)
        self.y_opt = bo.actual_fun(0, opt.x)
        self.err = []
        self.max_af = []
        
        # Prepare plot
        self.fig = plt.figure(constrained_layout=True, figsize=(12, 8))
        gs = gridspec.GridSpec(2, 2)
        self.ax_learn = self.fig.add_subplot(gs[0, 0])
        self.ax_err = self.fig.add_subplot(gs[0, 1])
        self.ax_img_c = self.fig.add_subplot(gs[1, 0])
        self.ax_img_y = self.fig.add_subplot(gs[1, 1])

        self.ax_learn.set_title('Learning')
        self.ax_learn.set_xlabel(r'compensation $c$')
        self.ax_learn.set_ylabel(r'value $v$')

        self.ax_err.set_title('Regret')
        self.ax_err.set_xlabel(r'round $k$')
        self.ax_err.set_ylabel(r'$y_{opt} - y$'+'\nmax EI')
        self.ax_err.set_xlim(0, num_rounds)

        self.ax_img_c.set_title('Compensations in previous round')
        self.ax_img_y.set_title('Values in previous round')
        
        self.X_plt = np.linspace(*bo.domain, num=500)
        
        # Draw known function
        (self.line_fun, ) = self.ax_learn.plot(self.X_plt, bo.actual_fun(0, self.X_plt), 'r--', label='Actual Value Function')
        self.ax_learn.set_xlim(*bo.domain)
        
        y_upper_bound = minimize_scalar(lambda x: bo.actual_fun(0, x), method='bounded', bounds=bo.domain).fun
        self.ax_learn.set_ylim(-0.05, 1.5 * y_upper_bound)

        self.ax_learn.legend()

        # Initial max max AF value
        f_af = lambda x: -bo.AF(x, np.max(bo.memory_y), bo.mu, bo.sigma_sq)
        res = minimize_scalar(f_af, method='bounded', bounds=bo.domain)

        self.max_af.append(-res.fun)
        (self.line_max_af, ) = self.ax_err.plot([0], self.max_af, c='y', lw=2, label='max AF value')

        self.err.append(self.y_opt - bo.memory_y[-1])
        (self.line_err, ) = self.ax_err.plot([0], self.err, c='r', lw=2, label='error')
        
        self.ax_err.legend()

        self.line_mean, self.line_seen_pts, self.line_last_pt, self.line_max_pt, self.std_dev_poly = draw_posterior(self.ax_learn, self.X_plt, bo.mu_prior, lambda x: bo.K(x, x), 
                                                                              bo.memory_X, bo.memory_y)
        self.af_line = draw_af(self.ax_learn, self.X_plt, bo.AF, bo)

        self.img_c, self.img_y = draw_img(self.ax_img_c, self.ax_img_y, S_c_init, S_y_init, *bo.domain)


        self.fig.canvas.draw()

        self.plot_interval = plot_interval
        self.refresh_time = refresh_time
        self.num_rounds = num_rounds

    def update_plot(self, bo, k, new_x, new_obs, max_af_val, S_c_cur, S_y_cur):
        self.ax_learn.set_title(f'Round: {k}')

        if k % self.plot_interval == 0:
            self.std_dev_poly = redraw_posterior(self.ax_learn, self.X_plt, bo.mu, bo.sigma_sq, bo.memory_X, bo.memory_y, 
                             self.line_mean, self.line_seen_pts, self.line_last_pt, self.line_max_pt, self.std_dev_poly, self.line_fun, bo.actual_fun(k, self.X_plt))

        



        self.max_af.append(max_af_val)
        self.line_max_af.set_data(range(k+1), self.max_af)

        self.err.append(self.y_opt - bo.memory_y[-1])
        self.line_err.set_data(range(k+1), self.err)

        self.ax_learn.relim()
        self.ax_learn.autoscale_view()
        self.ax_err.autoscale_view()
        
        if k % self.plot_interval == 0:
            redraw_af(self.af_line, self.X_plt, bo.AF, bo)
            redraw_img(self.img_c, self.img_y, S_c_cur, S_y_cur)

            if not bo.explore:
                self.line_last_pt.set_data(new_x, new_obs)

            # Pause already calls draw
            plt.pause(self.refresh_time)
            self.fig.canvas.flush_events()

            

        print(80*'-')
        print('Finished!')
    

def plot_learning(bo, rounds=5, domain=(1, 30), refresh_time=0.1, plot_interval=10, verbose=True, v_interval=100):
    '''Error can be negative because of noise'''
    idx_best_seen = None
    bo.explore = True

    opt = minimize_scalar(lambda x: -bo.actual_fun(0, x), method='bounded', bounds=domain)
    y_opt = bo.actual_fun(0, opt.x)
    err = []
    max_af = []
    
    # Prepare plot
    fig, (ax_learn, ax_err) = plt.subplots(1, 2, constrained_layout=True, figsize=(12, 4))

    ax_learn.set_title('Learning')
    ax_learn.set_xlabel(r'compensation $c$')
    ax_learn.set_ylabel(r'value $v$')

    ax_err.set_title('Regret')
    ax_err.set_xlabel(r'round $k$')
    ax_err.set_ylabel(r'$y_{opt} - y$'+'\nmax EI')
    ax_err.set_xlim(0, rounds)
    
    X_plt = np.linspace(*domain, num=500)
    
    # Draw known function
    (line_fun, ) = ax_learn.plot(X_plt, bo.actual_fun(0, X_plt), 'r--', label='Actual Value Function')
    ax_learn.set_xlim(*domain)

    y_upper_bound = -minimize_scalar(lambda x: -bo.actual_fun(0, x), method='bounded', bounds=bo.domain).fun
    ax_learn.set_ylim(-0.05, 1.5 * y_upper_bound)

    ax_learn.legend()

    # Initial max max AF value
    f_af = lambda x: -bo.AF(x, np.max(bo.memory_y), bo.mu, bo.sigma_sq)
    res = minimize_scalar(f_af, method='bounded', bounds=domain)

    max_af.append(-res.fun)
    (line_max_af, ) = ax_err.plot([0], max_af, c='y', lw=2, label='max AF value')

    err.append(y_opt - bo.memory_y[-1])
    (line_err, ) = ax_err.plot([0], err, c='r', lw=2, label='error')
    
    ax_err.legend()

    line_mean, line_seen_pts, line_last_pt, line_max_pt, std_dev_poly = draw_posterior(ax_learn, X_plt, bo.mu_prior, lambda x: bo.K(x, x), 
                                                                          bo.memory_X, bo.memory_y)
    af_line = draw_af(ax_learn, X_plt, bo.AF, bo)


    fig.canvas.draw()
    for k in range(1, rounds):
        
        bo.update_posterior()
        
        ax_learn.set_title(f'Round: {k}')

        if k % plot_interval == 0:
            std_dev_poly = redraw_posterior(ax_learn, X_plt, bo.mu, bo.sigma_sq, bo.memory_X, bo.memory_y, 
                             line_mean, line_seen_pts, line_last_pt, line_max_pt, std_dev_poly, line_fun, bo.actual_fun(k, X_plt))

        new_x, new_obs, max_af_val = bo.next(k)



        max_af.append(max_af_val)
        line_max_af.set_data(range(k+1), max_af)

        err.append(y_opt - bo.memory_y[-1])
        line_err.set_data(range(k+1), err)

        ax_err.relim()
        ax_err.autoscale_view()
        
        if k % plot_interval == 0:
            redraw_af(af_line, X_plt, bo.AF, bo)

            if not bo.explore:
                line_last_pt.set_data(new_x, new_obs)

            # Pause already calls draw
            plt.pause(refresh_time)
            fig.canvas.flush_events()

            
        if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{rounds}', end='\r')

    print(80*'-')
    print('Finished!')
    
    return (bo.assigned_X, bo.assigned_y)


def plot_zooming(zooming, actual_fun, rounds=5, domain=(1, 30), refresh_time=0.1, plot_interval=10, verbose=True, v_interval=100):
    '''Error can be negative because of noise'''
    idx_best_seen = None

    opt = minimize_scalar(lambda x: -actual_fun(0, x), method='bounded', bounds=domain)
    y_opt = actual_fun(0, opt.x)
    err = []
    
    # Prepare plot
    fig, (ax_learn, ax_err) = plt.subplots(1, 2, constrained_layout=True, figsize=(12, 4))

    ax_learn.set_title('Learning')
    ax_learn.set_xlabel(r'compensation $c$')
    ax_learn.set_ylabel(r'value $v$')

    ax_err.set_title('Regret')
    ax_err.set_xlabel(r'round $k$')
    ax_err.set_ylabel(r'$y_{opt} - y$')
    ax_err.set_xlim(0, rounds)
    
    X_plt = np.linspace(*domain, num=500)
    
    # Draw known function
    (line_fun, ) = ax_learn.plot(X_plt, actual_fun(0, X_plt), 'r--', label='Actual Value Function')
    ax_learn.set_xlim(*domain)

    (line_seen_pts, ) = ax_learn.plot(0, 0, marker='o', markersize=7, color='green', linestyle='None', label='seen points')
    (line_last_pt, ) = ax_learn.plot(0, 0, marker='o', markersize=13, color='orange', linestyle='None', label='last point')

    y_upper_bound = -minimize_scalar(lambda x: -actual_fun(0, x), method='bounded', bounds=domain).fun
    ax_learn.set_ylim(-0.05, 1.5 * y_upper_bound)

    ax_learn.legend()

    (line_err, ) = ax_err.plot([0], [0], c='r', lw=2, label='error')
    
    ax_err.legend()

    fig.canvas.draw()
    for k in range(1, rounds):
                
        ax_learn.set_title(f'Round: {k}')

        new_x, new_obs = zooming.next(k)

        err.append(y_opt - zooming.assigned_y[-1])
        line_err.set_data(range(k), err)

        ax_err.relim()
        ax_err.autoscale_view()
        
        if k % plot_interval == 0:
            line_fun.set_data(X_plt, actual_fun(k, X_plt))
            line_seen_pts.set_data(zooming.assigned_X[:-1], zooming.assigned_y[:-1])
            line_last_pt.set_data(zooming.assigned_X[-1], zooming.assigned_y[-1])

            # Pause already calls draw
            plt.pause(refresh_time)
            fig.canvas.flush_events()

            
        if verbose and (k % v_interval == 0):
                print(f'Round: {k:7}/{rounds}', end='\r')

    print(80*'-')
    print('Finished!')
    
    return (zooming.assigned_X, zooming.assigned_y)



def plot_all_worker_funs(W, domain, k=0):
        '''
        Args:
            save_fig (bool): if true ax is part of the newly created figure that is saved
        '''
        X = np.linspace(*domain, num=5_000)
        fig, ax = plt.subplots(1, 1)
        for i in range(W.shape[0]):
            for j in range(W.shape[1]):
                ax.plot(X, W[i, j](k, X), label=f'{i, j}')
        ax.set_title('Actual worker-task value function')
        ax.set_xlabel('compensation')
        ax.set_ylabel('value')
        # ax.legend()

        plt.show()