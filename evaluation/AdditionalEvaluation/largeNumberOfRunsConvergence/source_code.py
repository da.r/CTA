from pathlib import Path
from copy import deepcopy

import shutil
import timeit
import pandas as pd

from sklearn.gaussian_process import kernels

from environment_manager import *
from workers import *
from task_assignment import *
from acquisition import *

from exp3_crowdtas import EXP3CrowdTAs
from bwk_crowdtas import BwKCrowdTAs

from visual.tasp import TaskAssignmentSaverPlotter


random_state = 500

# Scenario Configuration
RUNS = 1
N = 10
M = 5
rounds = 10_000
domain = (5, 30)
assure_iden_comp=False
keep_in_memory = None

plot = None

# Worker generation settings
worker_funs = (bounded_logistic_cost, )
param_bounds = (((5, 29.5), (0.1, 3), (0.1, 1), (3, 25)), )
y_noise_fun = lambda prev_res: prev_res + rng.uniform(-0.1, 0.1)
time_change_fun_no_noise = lambda k, x: x

time_change_prob = 0.5

# Dynamic scenario
# same_over_rounds = False
# time_change_fun = lambda k, x: x + (k > 1500)*5 + (k > 2500)*(0.005)*(k-3000)

# # Static scenario
same_over_rounds = True
time_change_fun = lambda k, x: x


# Value function optimization

# Bayesian optimization
bayopt_args = dict(
	mu_prior = lambda X: 0.0 * np.ones_like(X),
	kernel = 0.5 * kernels.RBF(3) +  kernels.WhiteKernel(noise_level=0.01),
	noise = 0.05,
	discretization = 2,

	win_size = 50,
	stats_memory_size=10,

	AF = EI,

	loc_expl_rate=0.2,
	loc_expl=0.8,
	loc_expl_jump=0.1,
	loc_expl_min=0.05,
	loc_expl_max=0.8,
	glob_expl_rate=0.2,
	glob_expl=0.5,
	glob_expl_jump=0.1,
	glob_expl_min=0.05,
	glob_expl_max=1,

	opt_pt_mean_fraction = 2,
	opt_pt_std_fraction = 1,
	opt_restarts = 5,

	estimate_hyperparams = True,
	hyperparam_lr = 0.001,
	reset_hyperparams = False,

	use_smw = False
)


# Zooming algorithm
zoom_args = dict(
	conf_importance = 0.05,
	init_active = 2,
	neighborhoods = 20
)


# Task assignment
TA_rule = 'max_prev+forget'
TA_rule_win = 50
TA_rule_discount_factor = 0.95
wait_expl_rate = 0.0001

# Worker selection
# BwK parameters
budgets = np.ones(M) * 15_000_000
workers_per_task = np.array([2, 2, 2, 2, 2])
# task_priorities = np.arange(M)[::-1]
task_priorities = None
must_assign_all = False

# For Load Balancing
# lb_profiles = [np.array([0.6, 0.1, 0.1, 0.1, 0.1]), ]
# lb_profiles = [np.array([0.2, 0.2, 0.2, 0.2, 0.2]), ]

# Crowdsourcing process which repeats
lb_profiles = [np.array([0.2, 0.2, 0.2, 0.2, 0.2]),
			   np.array([0.6, 0.1, 0.1, 0.1, 0.1]),
			   np.array([0.2, 0.1, 0.5, 0.1, 0.1]), ]

if __name__ == '__main__':
	rng = np.random.default_rng()
	run_id = f'{rng.choice(1_000_000):06}'

	path = (Path.cwd() / 'results' / f'run_{run_id}').resolve()
	path.mkdir(parents=True, exist_ok=True)

	# Copy this file which contains data with all of the run settings:
	shutil.copy2('./evaluation.py', (path / f'source_code.py').resolve())


	tta = TaskAssignmentSaverPlotter(path, run_id, N, M, rounds, domain, TA_rule, TA_rule_win, TA_rule_discount_factor,
									 time_change_fun, time_change_prob, y_noise_fun, param_bounds, bayopt_args,
									 zoom_args, random_state)


	W, W_no_noise = generate_workers(N, M, funs=worker_funs, 
						 param_bounds=param_bounds, 
						 verbose=True, time_change_fun=time_change_fun, without_noise_fun=time_change_fun_no_noise, y_noise_fun=y_noise_fun, time_change_prob=time_change_prob, random_state=random_state)

	tta.plot_all_worker_funs(W_no_noise)
	# Plot only in a dynamic scenario
	if not same_over_rounds:
		tta.plot_all_worker_funs(W_no_noise, k=1600)
		tta.plot_all_worker_funs(W_no_noise, k=4500)
	
	# Instantiation
	oa = OptimalAssignment(*W.shape, rounds, W_no_noise, W, domain, same_over_rounds=same_over_rounds, verbose=True)
	# When it appears before CrowdTAs implementations it gets corrupted when kernels are reset in BayOpt
	oa_c, oa_y, assign_history, comp_history, opt_C, opt_y = oa.run(verbose=True, return_history=True)

	ra_opt_all = RandomAssignment(*W.shape, rounds, W, domain=domain, C=opt_C, assure_iden_comp=assure_iden_comp, random_state=random_state)
	rr_opt_all = RRAssignment(*W.shape, rounds, W, domain=domain, C=opt_C, assure_iden_comp=assure_iden_comp, random_state=random_state)

	# LB specific baseline and optimal
	lb_oa = OptimalAssignment(*W.shape, rounds, W_no_noise, W, domain, lb_profiles=lb_profiles, same_over_rounds=same_over_rounds, verbose=True)
	lb_oa_c, lb_oa_y, lb_assign_history, lb_comp_history, lb_opt_C, lb_opt_y = lb_oa.run(verbose=True, return_history=True)

	lb_ra_opt_all = RandomAssignment(*W.shape, rounds, W, domain=domain, C=lb_opt_C, assure_iden_comp=assure_iden_comp, lb_profiles=lb_profiles, random_state=random_state)
	
	# Instantiation takes some time. One can comment unused in `eval_objs`.
	bo_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
						   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
			  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
			  	   		   VFO_method='bayopt', TA_method='adex', must_assign_all=must_assign_all, 
			  	   		   random_state=random_state)
	bo_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
						   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
			  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
			  	   		   VFO_method='bayopt', TA_method='ucb', must_assign_all=must_assign_all, 
			  	   		   random_state=random_state)
	zoom_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, keep_in_memory=keep_in_memory, 
						   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
			  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
			  	   		   VFO_method='zooming', TA_method='adex', must_assign_all=must_assign_all, 
			  	   		   random_state=random_state)
	zoom_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, keep_in_memory=keep_in_memory, 
						   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
			  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
			  	   		   VFO_method='zooming', TA_method='ucb', must_assign_all=must_assign_all, 
			  	   		   random_state=random_state)

	aic_bo_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
								domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   		TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   		VFO_method='bayopt', TA_method='adex', must_assign_all=must_assign_all, 
					  	   		random_state=random_state)
	aic_bo_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='bayopt', TA_method='ucb', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	aic_zoom_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='zooming', TA_method='adex', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	aic_zoom_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='zooming', TA_method='ucb', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)

	lb_bo_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
								domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   		TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   		VFO_method='bayopt', TA_method='adex', must_assign_all=must_assign_all, 
					  	   		random_state=random_state)
	lb_bo_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='bayopt', TA_method='ucb', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	lb_zoom_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='zooming', TA_method='adex', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	lb_zoom_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='zooming', TA_method='ucb', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	
	aic_lb_bo_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
								domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   		TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   		VFO_method='bayopt', TA_method='adex', must_assign_all=must_assign_all, 
					  	   		random_state=random_state)
	aic_lb_bo_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='bayopt', TA_method='ucb', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	aic_lb_zoom_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='zooming', TA_method='adex', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)
	aic_lb_zoom_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, lb_profiles=lb_profiles, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=True, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
					  	   	   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  	   	   VFO_method='zooming', TA_method='ucb', must_assign_all=must_assign_all, 
					  	   	   random_state=random_state)


	exp3_cta = EXP3CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
							domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
			  	   		    TA_rule_discount_factor=TA_rule_discount_factor, use_best_next=False, 
			  	   		    wait_expl_rate=wait_expl_rate, random_state=random_state)

	exp3pp_cta = EXP3CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
							  domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, 
							  TA_rule_win=TA_rule_win, TA_rule_discount_factor=TA_rule_discount_factor, 
							  wait_expl_rate=wait_expl_rate, use_best_next=False, alg={'name': 'exp3++'}, 
							  random_state=random_state)


	bwk = BwKCrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, 
					  budgets=budgets, task_priorities=task_priorities, workers_per_task=workers_per_task,
					  domain=domain, assure_iden_comp=False, TA_rule=TA_rule, TA_rule_win=TA_rule_win, 
					  TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
					  random_state=random_state)

	# EXTEND:
	# Create an instance of the approach here, e.g.:
	# bo_ucb_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  bayopt_args, keep_in_memory=keep_in_memory, 
	# 					   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
	# 		  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
	# 		  	   		   VFO_method='bayopt', TA_method='ucb', must_assign_all=must_assign_all, 
	# 		  	   		   random_state=random_state)


	ra_opt_all_c, ra_opt_all_y = ra_opt_all.run(verbose=True)
	rr_opt_all_c, rr_opt_all_y = rr_opt_all.run(verbose=True)
	lb_ra_opt_all_c, lb_ra_opt_all_y = lb_ra_opt_all.run(verbose=True)

	# Shape: (key=label, value=[Assignment instance, (S_c, S_y), (Average S_c[-1] over RUNS, Average S_y[-1] over RUNS)])
	base_eval_objs = {
		'Optimal': [('g', '-'), None, (oa_c, oa_y), None],
		'LB Optimal': [('seagreen', 'x-'), None, (lb_oa_c, lb_oa_y), None],
		# 'Round Robin': [('gold', '-'), None, (rr_opt_all_c, rr_opt_all_y), None],
		'LB Random': [('lightgreen', '-'), None, (lb_ra_opt_all_c, lb_ra_opt_all_y), None],
	}
	eval_objs = {
		'Zoom': [('r', '-'), zoom_adex_cta],
		# 'Zoom UCB': [('tomato', '-x'), zoom_ucb_cta],
		'BayOpt': [('b', '-'), bo_adex_cta],
		# 'BayOpt UCB': [('cornflowerblue', '-x'), bo_ucb_cta],

		# 'Zoom AIC': [('darkred', '-o'), aic_zoom_adex_cta],
		# 'Zoom UCB AIC': [('firebrick', '-x'), aic_zoom_ucb_cta],
		# 'BayOpt AIC': [('darkblue', '-o'), aic_bo_adex_cta],
		# 'BayOpt UCB AIC': [('lightsteelblue', '-x'), aic_bo_ucb_cta],
		
		'Zoom LB': [('brown', '-x'), lb_zoom_adex_cta],
		# 'Zoom UCB LB': [('lightcoral', '-x'), lb_zoom_ucb_cta],
		'BayOpt LB': [('indigo', '-x'), lb_bo_adex_cta],
		# 'BayOpt UCB LB': [('blueviolet', '-x'), lb_bo_ucb_cta],

		# 'Zoom LB AIC': [('orange', '-s'), aic_lb_zoom_adex_cta],
		# 'Zoom UCB LB AIC': [('moccasin', '-x'), aic_lb_zoom_ucb_cta],
		# 'BayOpt LB AIC': [('lime', '-s'), aic_lb_bo_adex_cta],
		# 'BayOpt UCB LB AIC': [('springgreen', '-x'), aic_lb_bo_ucb_cta],
		
		# 'EXP3': [('darkgrey', '-'), exp3_cta],
		# 'EXP3++': [('dimgrey', '-x'), exp3pp_cta],
		
		# 'BwK CTA': [('black', '-'), bwk],

		# EXTEND: 
		# Further approaches that implement the `TaskAssignment` abstract class can be added for evaluation, one has to
		# specify here label and instance, e.g.;
		# 'BayOpt UCB CrowdTAs': [('red', '-'), bo_ucb_cta],
	}
	
	# Start after optimal and baselines
	for label in eval_objs:
		start = timeit.default_timer()
		tmp_cs = []
		tmp_ys = []
		plot_cs = []
		plot_ys = []

		_, run_c, run_y = deepcopy(eval_objs[label][1]).run(verbose=True, v_interval=5, opt_assign=assign_history, opt_comp=comp_history, plot=plot)
		tmp_cs.append(np.sum(run_c))
		tmp_ys.append(np.sum(run_y))
		# To plot average over runs
		plot_cs.append(run_c)
		plot_ys.append(run_y)
		# To plot a single run append the first run for plotting
		# eval_objs[label].append((run_c, run_y))
		for i in range(RUNS - 1):
			_, run_c, run_y = deepcopy(eval_objs[label][1]).run(verbose=True, v_interval=5, opt_assign=assign_history, opt_comp=comp_history, plot=plot)
			tmp_cs.append(np.sum(run_c))
			tmp_ys.append(np.sum(run_y))
			plot_cs.append(run_c)
			plot_ys.append(run_y)

		eval_objs[label].append((sum(plot_cs) / RUNS, sum(plot_ys) / RUNS))

		avg_c = sum(tmp_cs) / RUNS
		avg_y = sum(tmp_ys) / RUNS
		eval_objs[label].append((avg_c, avg_y))
		stop = timeit.default_timer()
		print(f'Finished {RUNS} runs of `{label}`')


	all_evals = {**base_eval_objs, **eval_objs}

	# Plot time-budget, time-value and budget-value
	fig, axs = plt.subplots(1,3, figsize=(16, 4))
	tta.plot_eval(fig, axs, all_evals, plot_type='avgValUntilK', optimal_y=oa_y, save_figs=3*[True], fontsize='large', markevery=0.1)
	fig, axs = plt.subplots(1,3, figsize=(16, 4))
	tta.plot_eval(fig, axs, all_evals, plot_type='cumulativeRegret', optimal_y=oa_y, save_figs=3*[True], fontsize='large', markevery=0.1)

	# Useful only if single algorithm is used
	# fig, axs = plt.subplots(1,3, figsize=(16, 4))
	# tta.plot_eval(fig, axs, eval_objs, plot_type='instantValueRatio', optimal_y=oa_y, save_figs=3*[True], fontsize='small')

	stats_df = pd.DataFrame(columns=['Method', 'Spent', 'Cumulative Value'])
	for label in all_evals:
	    stats_df.loc[len(stats_df)] = [label, np.sum(all_evals[label][2][0]), np.sum(all_evals[label][2][1])]

	path.mkdir(parents=True, exist_ok=True)
	stats_df.to_latex((path / 'experiment_results.tex').resolve(), index=False, float_format='{:,.2f}'.format)