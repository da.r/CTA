from pathlib import Path
from copy import deepcopy

import shutil
import timeit
import pandas as pd

from sklearn.gaussian_process import kernels

from environment_manager import *
from workers import *
from task_assignment import *
from acquisition import *

from exp3_crowdtas import EXP3CrowdTAs
from bwk_crowdtas import BwKCrowdTAs

from visual.tasp import TaskAssignmentSaverPlotter


random_state = 1000

# Scenario Configuration
RUNS = 5
N = 1
M = 1
rounds = 5_000
domain = (5, 30)
assure_iden_comp=False
keep_in_memory = None

plot = None

# Worker generation settings
worker_funs = (bounded_logistic_cost, )
param_bounds = (((5, 29.5), (0.1, 3), (0.1, 1), (3, 25)), )
y_noise_fun = lambda prev_res: prev_res + rng.uniform(-0.1, 0.1)
time_change_fun_no_noise = lambda k, x: x

time_change_prob = 1

# Value function optimization

# Bayesian optimization
bayopt_args = dict(
	mu_prior = lambda X: 0.0 * np.ones_like(X),
	kernel = 0.5 * kernels.RBF(3) +  kernels.WhiteKernel(noise_level=0.01),
	noise = 0.05,
	discretization = 2,

	win_size = 50,
	stats_memory_size=10,

	AF = EI,

	loc_expl_rate=0.2,
	loc_expl=0.8,
	loc_expl_jump=0.1,
	loc_expl_min=0.05,
	loc_expl_max=0.8,
	glob_expl_rate=0.2,
	glob_expl=0.5,
	glob_expl_jump=0.1,
	glob_expl_min=0.05,
	glob_expl_max=1,

	opt_pt_mean_fraction = 2,
	opt_pt_std_fraction = 1,
	opt_restarts = 5,

	estimate_hyperparams = True,
	hyperparam_lr = 0.001,
	reset_hyperparams = False,

	use_smw = False
)


# Zooming algorithm
zoom_args = dict(
	conf_importance = 0.05,
	init_active = 2,
	neighborhoods = 20,
	L = 1
)


# Task assignment
TA_rule = 'max_prev+forget'
TA_rule_win = 50
TA_rule_discount_factor = 0.95
wait_expl_rate = 0.0001

# Worker selection
# BwK parameters
budgets = np.ones(M) * 15_000_000
workers_per_task = np.array([2, 2, 2, 2, 2])
# task_priorities = np.arange(M)[::-1]
task_priorities = None
must_assign_all = False

# For Load Balancing
# lb_profiles = [np.array([0.6, 0.1, 0.1, 0.1, 0.1]), ]
lb_profiles = [np.array([0.2, 0.2, 0.2, 0.2, 0.2]), ]

# Crowdsourcing process which repeats
# lb_profiles = [np.array([0.2, 0.2, 0.2, 0.2, 0.2]),
# 			   np.array([0.6, 0.1, 0.1, 0.1, 0.1]),
# 			   np.array([0.2, 0.1, 0.5, 0.1, 0.1]), ]

if __name__ == '__main__':
	rng = np.random.default_rng()
	run_id = f'{rng.choice(1_000_000):06}'

	path = (Path.cwd() / 'results' / f'run_{run_id}').resolve()
	path.mkdir(parents=True, exist_ok=True)

	# Copy this file which contains data with all of the run settings:
	shutil.copy2('./evaluation.py', (path / f'source_code.py').resolve())

	# # Static scenario
	same_over_rounds = True
	time_change_fun = lambda k, x: x

	W, W_no_noise = generate_workers(N, M, funs=worker_funs, 
						 param_bounds=param_bounds, 
						 verbose=True, time_change_fun=time_change_fun, 
						 without_noise_fun=time_change_fun_no_noise, 
						 y_noise_fun=y_noise_fun, time_change_prob=time_change_prob, random_state=random_state)

	# Instantiation
	oa = OptimalAssignment(*W.shape, rounds, W_no_noise, W, domain, same_over_rounds=same_over_rounds, verbose=True)
	# When it appears before CrowdTAs implementations it gets corrupted when kernels are reset in BayOpt
	oa_c, oa_y, assign_history, comp_history, opt_C, opt_y = oa.run(verbose=True, return_history=True)

	param_vals = np.arange(2, 200, 5)
	values = []
	for param in param_vals:
		tmp = []
		for _ in range(RUNS):
			print(param)
			zoom_args['neighborhoods'] = param
			zoom_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
				  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
				  	   		   VFO_method='zooming', TA_method='adex', must_assign_all=must_assign_all, 
				  	   		   random_state=random_state)
			_, run_c, run_y = zoom_adex_cta.run(verbose=True, v_interval=5, opt_assign=assign_history, opt_comp=comp_history, plot=plot)
			tmp.append(run_y)
		values.append(np.sum(run_y) / RUNS)

	fig, ax = plt.subplots(1, 1, figsize=(16, 4))
	ax.set_xlabel(r'$|S_{all}|$', fontsize='large')
	ax.set_ylabel('average cumulative value', fontsize='large')
	ax.plot(param_vals, values, label='static')

	# Dynamic scenario
	same_over_rounds = False
	time_change_fun = lambda k, x: x + (k > 1500)*5 + (k > 2500)*(0.005)*(k-3000)

	W, W_no_noise = generate_workers(N, M, funs=worker_funs, 
						 param_bounds=param_bounds, 
						 verbose=True, time_change_fun=time_change_fun, 
						 without_noise_fun=time_change_fun_no_noise, 
						 y_noise_fun=y_noise_fun, time_change_prob=time_change_prob, random_state=random_state)

	# Instantiation
	oa = OptimalAssignment(*W.shape, rounds, W_no_noise, W, domain, same_over_rounds=same_over_rounds, verbose=True)
	# When it appears before CrowdTAs implementations it gets corrupted when kernels are reset in BayOpt
	oa_c, oa_y, assign_history, comp_history, opt_C, opt_y = oa.run(verbose=True, return_history=True)

	param_vals = np.arange(2, 200, 5)
	values = []
	for param in param_vals:
		tmp = []
		for _ in range(RUNS):
			print(param)
			zoom_args['neighborhoods'] = param
			zoom_adex_cta = CrowdTAs(*W.shape, rounds, W_no_noise, W,  zoom_args, keep_in_memory=keep_in_memory, 
							   domain=domain, assure_iden_comp=assure_iden_comp, TA_rule=TA_rule, TA_rule_win=TA_rule_win,
				  	   		   TA_rule_discount_factor=TA_rule_discount_factor, wait_expl_rate=wait_expl_rate, 
				  	   		   VFO_method='zooming', TA_method='adex', must_assign_all=must_assign_all, 
				  	   		   random_state=random_state)
			_, run_c, run_y = zoom_adex_cta.run(verbose=True, v_interval=5, opt_assign=assign_history, opt_comp=comp_history, plot=plot)
			tmp.append(run_y)
		values.append(np.sum(run_y) / RUNS)

	ax.plot(param_vals, values, label='dynamic')
	ax.legend()


	fig.savefig((path / f'evaluation.png'), bbox_inches='tight')
	plt.show()

	print(param_vals[np.argmax(values)])