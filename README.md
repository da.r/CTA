<img alt="CTA logo" src="logo.gif"  width="25%">

---

CTA is a testing environment for online heterogenous task assignment approaches which involve representation of worker-task pairs as noisy functions.

## Structure
- `/cta`: contains implementation or the algorithms and the environment
- `/evaluation`: contains experiments
- `/notebooks`: contain exploration of some elements of the implementation


## Usage
To generate a set of worker models and evaluate the solution one can either use an example found in `/cta/evaluation.py` with predefined schema and scenarios or do the following:
1. Choose one of the available utility function generators in `utility.py` and combine them with a compensation function or use one of the value function generators from `values.py` that by default use linear compensation function.
2. Through definition of `time_shift_function` decide on the way the value functions change over time and create a static or a dynamic scenario.
3. Generate the desired number of worker-task pair value functions. Thereby, one should also decide on the parameter bounds of the used value function generator. These bounds define a range of possible shapes of value functions. Other generator settings like the probability of change could also be considered.
4. Implement the solution which inherits from the `Assignment` abstract class.  Include the algorithm in `eval_objs` and uncomment the required baseline and optimal settings in `base_eval_objs`, or other settings in `eval_objs`. Thereby, one has to take into account that some of the settings such as those including `LB, LB AIC, AIC` require the number of worker N to be larger than M, the number of tasks.


The results of each run are generated under path `/cta/results/run_<run_id>`. Generated evaluation results have the following structure:
- `/avgValUntilK`: plots of average value until round K (spent-value plot, time-spent plot, time-value plot and an `evaluation.png` containing all the previous plots in one figure)
- `/cumulativeRegret`: cumulative regret plots (spent-value plot, time-spent plot, time-value plot and an `evaluation.png` containing all the previous plots in one figure)
- `<run_id>_k=0_workers.png`: an image of all of the generated worker-task value functions. If dynamic scenario is used, additional plots at other rounds are generated.
- `experiment_results.tex`: table which contains the amount of issued compensation and cumulative value after all of the rounds in the run
- `source_code.py`: contains evaluation source code containing all of the settings used to generate the results


### Dependencies
```
numpy
matplotlib
scipy
scikit-learn
pandas
```

To install dependencies one can call requirements file with path `/cta/requirements.txt`:
- ```pip install -r requirements.txt```

### Bayesian Optimization Value Function Optimization Visualization
= with uniform noise around the actual function

<img alt="BayOpts Inspection" src="inspection.gif"  width="100%">
